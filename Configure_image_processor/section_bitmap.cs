﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Configure_image_processor
{
    public partial class section_bitmap : Form
    {
        //public global
        string imageFileName;
        selectedArea selectionOfFocus;
        public  Match matchData = new Match();
        public  Size sizeOnExit;
        //private global
        private Size sizeToRefactorPointsFromSaveContext;
        private Point mouseDownPoint, mouseUpPoint;
        private Size PICTUREBOX_SIZE_GLOBAL;
        private bool mouseIsDown_GLOBAL, keyDown_GLOBAL;
        private DateTime keyDownTime_GLOBAL;
        private TimeSpan keyDownTimeSpan_GLOBAL;
        private int MOVEMENT_SPEED = 1; //how fast the arrow key moves the selection
        private bool matchDataWasLoadedInFlag = false;

        /*------------------Pixel Color Collection----------------------*/
        [DllImport("Gdi32.dll")]
        public static extern int GetPixel(
        System.IntPtr hdc,    // handle to DC
        int nXPos,  // x-coordinate of pixel
        int nYPos   // y-coordinate of pixel
        );

        /*[DllImport("User32.dll")]

        public static extern IntPtr GetDC(IntPtr wnd);

        [DllImport("User32.dll")]
        public static extern void ReleaseDC(IntPtr dc);
        *********************************************/

        Pen theWatcherPenIs     = new Pen(Color.Cyan, 1);  //default box color
        Pen theTextPenIs        = new Pen(Color.Orange,1); //default OCR selection color
        Pen theHighlightedPenIs = new Pen(Color.Magenta);

        //default letter drawing
        Brush theBrush = new SolidBrush(Color.Magenta); 
        Font drawFont = new Font("Arial", 12);
        //ToolTip toolTip1;

        //list of the selections
        public List<selectedArea> selectionList = new List<selectedArea>();
        private Image img;
        
        enum tool_enum
        {
            SELECT_TOOL,
            EYEDROP_TOOL,
        };    

        enum direction 
        {
            RIGHT = 0,
            LEFT,
            UP,
            DOWN
        };

        public section_bitmap(string image_file, List<selectedArea> loaded_selections, Size loadedSize, Match loaded_match)
        {
            InitializeComponent();
            InitializeListView();   
        
            selectionList = loaded_selections;
            matchData = loaded_match;

            if (!loadedSize.IsEmpty) PICTUREBOX_SIZE_GLOBAL = loadedSize; //if loaded in with other data, it will have a size.
            else PICTUREBOX_SIZE_GLOBAL = pictureBox1.Size;

            this.imageFileName = image_file;          

        }

        private void InitializeListView()
        {            
            //populateMatchData(importMatchData());
        }

#region Event Handlers
        private void section_bitmap_Load(object sender, EventArgs e)
        {
            //setup menu bar
            
           this.Menu = new MainMenu();
           MenuItem item = new MenuItem("File");
           this.Menu.MenuItems.Add(item);
           item.MenuItems.Add("Load Object List", new EventHandler(load_object_menu_Click));
           //item.MenuItems.Add("Save", new EventHandler(Save_Click));
           //item.MenuItems.Add("Open", new EventHandler(Open_Click));
           //item = new MenuItem("Edit");
           //this.Menu.MenuItems.Add(item);
           //item.MenuItems.Add("Undo", new EventHandler(Undo_Click));
            

            //set image of the picturebox
            img = Image.FromFile(imageFileName);
            pictureBox1.Image = img;            
            //decide how it looks
           // renderPictureBox();
            keyDown_GLOBAL = false;
            setButtonActiveState(this.done_button, false);

            if (selectionList.Count > 0) //was there data loaded in?
            {
                //yes, perform field maintenance
                selectionOfFocus = selectionList[selectionList.Count - 1];
                setButtonActiveState(done_button, true); //renable done button
                updateLabelFields(selectionOfFocus.startPoint, selectionOfFocus.endPoint);
                updateOtherFields();
            }
            if(matchData.teams.Count > 0)//loaded in with data
            {
                matchDataWasLoadedInFlag = true;
                populateMatchData(matchData);
            }
            else matchDataWasLoadedInFlag = false;
        }

        private void load_object_menu_Click(object sender, EventArgs e)
        {
            loadObjectsFromXML(); 
        }



        #region Menu Item Events

        private void Undo_Click(object sender, EventArgs e)
        {
            removeLastSelection();
            clearDrawing();
            drawBoxes();
            updateOtherFields();
        }
        private void Open_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Mouse Events

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {

            setMouseState(true);

            switch (e.Button) 
            {
                case System.Windows.Forms.MouseButtons.Right:
                    mouseDownEyedropAction(e);
                    break;
                case System.Windows.Forms.MouseButtons.Left:
                    //determine click action
                    switch (radioState())
                    {
                        case (int)tool_enum.EYEDROP_TOOL:
                            mouseDownEyedropAction(e);
                            break;
                        case (int)tool_enum.SELECT_TOOL:
                        default:
                            mouseDownSelectAction(e);
                            break;
                    }
                    break;
                case System.Windows.Forms.MouseButtons.Middle:
                    switch (radioState())  //middle mouse click down
                    {
                        case (int)tool_enum.SELECT_TOOL: //select tool being used
                            if (locationIsInRectangle(selectionOfFocus.startPoint,selectionOfFocus.endPoint,e.Location))
                            {       //click was in the focused selection
                                toggleFocusOCRState();
                                updateOCRCheckbox();
                            }
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        private void picturebox_MouseMove(object sender, MouseEventArgs e)
        {
            updateCursorPosition(e.Location);

            switch (e.Button)
            {
                case System.Windows.Forms.MouseButtons.Right:
                    //do nothing special
                    break;
                case System.Windows.Forms.MouseButtons.Left:              
                    switch (radioState())
                    {
                        case (int)tool_enum.EYEDROP_TOOL:
                            mouseMoveEyedropAction(e); //does nohtin atm
                            break;
                        case (int)tool_enum.SELECT_TOOL:

                        default:
                            mouseMoveSelectAction(e); //performs redundant check on L or R button
                            break;
                    }
                    break;
                default:
                    break;
            }            
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {

            switch (e.Button)
            {
                case System.Windows.Forms.MouseButtons.Right:
                    //do nothing special
                    break;
                case System.Windows.Forms.MouseButtons.Left: //when left button, act normal
                    switch (radioState())
                    {
                        case (int)tool_enum.EYEDROP_TOOL:
                            mouseUpEyedropAction(e); //does nothing atm
                            break;
                        case (int)tool_enum.SELECT_TOOL:
                        default:
                            mouseUpSelectAction(e); //sets new selection
                            break;
                    }
                    break;
                default:
                    break;
            } 
        }



        #endregion

        #region Label Change Events
        private void xy_select_text_TextChanged(object sender, EventArgs e)
        {

        }

        private void wh_select_text_TextChanged(object sender, EventArgs e)
        {


        }

        private void color_select_text_TextChanged(object sender, EventArgs e)
        {

        }

        private void xy_selectText_leave(object sender, EventArgs e)
        {
            /* Feature Removed
            string myString = this.wh_select_text.Text;
            string[] stringValues = myString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            int x = Int32.Parse(stringValues[0]);
            int y = Int32.Parse(stringValues[1]);

            Point modifiedPoint = new Point(x, y);

            selectionList[selectionList.Count-1].endPoint = modifiedPoint;*/
        }
        #endregion

        #region Radio Change Events
        private void select_tool_radio_CheckedChanged(object sender, EventArgs e)
        {
            //pictureBox1.Cursor = Cursors.Cross;
        }

        private void eyedrop_tool_radio_CheckedChanged(object sender, EventArgs e)
        {
            //pictureBox1.Cursor = Cursors.Arrow;
        }
        #endregion

        #region Button Events

        private void undo_button_Click(object sender, EventArgs e)
        {
            removeLastSelection();
            clearDrawing();
            drawBoxes();
            updateOtherFields();
        }

        private void repaint_button_Click(object sender, EventArgs e)
        {
            drawBoxes();           
            updateOtherFields();
        }
        private void clear_selections_button_Click(object sender, EventArgs e)
        {
            while (selectionList.Count > 0) //for all selections; until selection index is back to its original value
            {
                removeLastSelection();                         //remove
            }
            clearDrawing();
            updateOtherFields();
        }

        private void done_button_Click(object sender, EventArgs e)
        {
            saveSize();
            transposeDataPoints(); //convert from what is seen to the actual pixels
            //img.Dispose();
            this.Close();
        }

        private void saveSize()
        {
            sizeOnExit = pictureBox1.Size;
        }
        private void collect_color_button_Click(object sender, EventArgs e)
        {
            setColorOfLastSelection(collectColor(getCenterOfRectangle(selectionOfFocus.startPoint, selectionOfFocus.endPoint)));
            updateColorField();
        }

        private void generate_item_list_button_Click(object sender, EventArgs e)
        {
            //serialize the saved objects.
            //get file basename
            FileInfo fi = new FileInfo(imageFileName);
            string picBaseDir = fi.Directory + "";
            string saveAs;

            using (var fDialog = new System.Windows.Forms.SaveFileDialog())
            {
                fDialog.Title = "Save Object As...";
                fDialog.InitialDirectory = picBaseDir;
                fDialog.DefaultExt = ".xml";
                fDialog.Filter = "Object XML|*.xml";

                if (fDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //Nothing for now...
                }

                saveAs = fDialog.FileName;
            }

            //create serializer stuff
            XmlSerializer ser = new XmlSerializer(typeof(Match));
            Match emptyMatch = new Match();
            Team emptyTeam1 = new Team();
            Team emptyTeam2 = new Team();
            indexedString freeAttributeTeam1 = new indexedString();
            indexedString freeAttributeTeam2 = new indexedString();
            freeAttributeTeam1.text = "Dire Team Name";
            emptyTeam1.attributes.Add(freeAttributeTeam1);//add attribute to team
            emptyMatch.teams.Add(emptyTeam1);//add team to match

            freeAttributeTeam2.text = "Radiant Team Name";
            emptyTeam2.attributes.Add(freeAttributeTeam2);//add attribute to team
            emptyMatch.teams.Add(emptyTeam2);//add team to match

            indexedString freeAttributePlayers = new indexedString();
            freeAttributePlayers.text = "Player Attribute";

            //the name
            for (int i = 0; i < global_constants.TEAMS_IN_MATCH;i++ )
            {
                Tower freeTowers = new Tower();
                for (int j = 0; j < global_constants.PLAYERS_ON_TEAM; j++)//all players
                {
                    Player freeAgent = new Player();                
                    for (int k = 0; k < global_constants.DATA_IN_PLAYER; k++)//all stats of the player
                    {
                        freeAgent.attributes.Add(freeAttributePlayers);
                    }
                    emptyMatch.teams[i].players.Add(freeAgent);
                }
                for (int l = 0; l < (global_constants.TOWERS_PER_TEAM); l++)
                {
                    emptyMatch.teams[i].towers.Add(freeTowers);
                }
            }

            if (saveAs.Length > 0)
            {
                Stream stream = new FileStream(saveAs, FileMode.Create, FileAccess.Write, FileShare.None);
                ser.Serialize(stream, emptyMatch);
                stream.Close();
            }
        }

        private void import_match_button_Click(object sender, EventArgs e)
        {
            loadObjectsFromXML();
        }

        #endregion

        #region Keyboard Events

        private void keypress_Event(object sender, KeyPressEventArgs e)
        {
            Console.WriteLine(e.KeyChar);
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            processKeyPress(keyData);
            return true;
        }

        private void processKeyPress(Keys keyData)
        {

           setMovementSpeed();
           moveSelection(keyData);
        }

        private void setMovementSpeed()
        {
            if (keyDown_GLOBAL) 
            {
                if (keyDownTimeSpan_GLOBAL.Milliseconds > 300)
                {
                    MOVEMENT_SPEED = 10;
                }
                else MOVEMENT_SPEED = 1;
            }
        }

        private void moveSelection(Keys keyData)
        {
            if(selectionOfFocus != null)
            {
                clearDrawing();
                Application.DoEvents();//helps with rendering - clears it immediately, so that it can be redrawn immediately.
                switch (keyData)
                {
                    case Keys.Left:
                        selectionOfFocus.startPoint = new Point((selectionOfFocus.startPoint.X - MOVEMENT_SPEED), 
                                                            selectionOfFocus.startPoint.Y);
                        selectionOfFocus.endPoint = new Point((selectionOfFocus.endPoint.X - MOVEMENT_SPEED),
                                                            selectionOfFocus.endPoint.Y);
                        break;
                    case Keys.Right:
                        selectionOfFocus.startPoint = new Point((selectionOfFocus.startPoint.X + MOVEMENT_SPEED), 
                                                            selectionOfFocus.startPoint.Y);
                        selectionOfFocus.endPoint = new Point((selectionOfFocus.endPoint.X + MOVEMENT_SPEED),
                                                            selectionOfFocus.endPoint.Y);
                        break;
                    case Keys.Up:
                        selectionOfFocus.startPoint = new Point(selectionOfFocus.startPoint.X,
                                                            (selectionOfFocus.startPoint.Y - MOVEMENT_SPEED));
                        selectionOfFocus.endPoint  = new Point(selectionOfFocus.endPoint.X,
                                                            (selectionOfFocus.endPoint.Y - MOVEMENT_SPEED));
                        break;
                    case Keys.Down:
                        selectionOfFocus.startPoint = new Point(selectionOfFocus.startPoint.X,
                                                            (selectionOfFocus.startPoint.Y + MOVEMENT_SPEED));
                        selectionOfFocus.endPoint  = new Point(selectionOfFocus.endPoint.X,
                                                            (selectionOfFocus.endPoint.Y + MOVEMENT_SPEED));
                        break;

                }
                drawBoxes();
                updateLabelFields(selectionOfFocus.startPoint, selectionOfFocus.endPoint);
                updateOtherFields();      
            }
        }


        #endregion

        #region Checkbox Events
        private void ocr_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (selectionOfFocus != null) selectionOfFocus.isOCR = this.ocr_checkbox.Checked;
        }
        private void lock_ratio_checkbox_Event(object sender, EventArgs e)
        {
            renderPictureBox();
        }

        #endregion

        #region Textbox Events
        private void threshold_text_TextChanged(object sender, EventArgs e)
        {
            if (this.threshold_text.Text.Length > 0) 
            {
                try 
                {
                    int userThresh = Int32.Parse(this.threshold_text.Text);
                    if (selectionOfFocus != null)
                    {
                        if (userThresh >= 0 && userThresh <= 255)
                        {
                            selectionOfFocus.colorThreshold = userThresh;
                        }
                        else
                        selectionOfFocus.colorThreshold = 30;//default to 30
                    }

                }
                catch 
                { 
                    MessageBox.Show("Please enter a valid  255 <= # => 0");
                }
            }
            
        }
        #endregion

        #region Form Change Events
        private void resize_Event(object sender, EventArgs e) 
        {

        }

        private void resize_picturebox_Event(object sender, EventArgs e)
        {
            //move the points...
            refactorDataPoints(PICTUREBOX_SIZE_GLOBAL, pictureBox1.Size);
            PICTUREBOX_SIZE_GLOBAL = pictureBox1.Size;
        }
        #endregion

        #region Drag Event


        private void item_object_drag_Event(object sender, ItemDragEventArgs e)
        {


        }


        #endregion

#endregion

        #region Mouse Event Actions

        private void mouseDownSelectAction(MouseEventArgs e)
        {
            pictureBox1.Cursor = Cursors.Cross;
            mouseDownPoint.X = e.X;
            mouseDownPoint.Y = e.Y;                      
        }

        private void mouseDownEyedropAction(MouseEventArgs e)
        {
            setMouseState(false);
            setColorOfLastSelection(collectColor(e.Location)); //collect the color of the pixel that was clicked & set the last selection's color.
            updateColorField(); //update the label
        }

        /****************************************************************************
         * FUNCTION mouseMoveSelectAction
         * 
         * Performs designated actions if mouseMove event occurs and the select tool is 
         * currently being used.
         * 
         * parameters:
         * mouseEventArg (mouse move event)
         * 
         * returns:
         * void
         * 
         ****************************************************************************/
        private void mouseMoveSelectAction(MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left) 
            {
                if (getMouseState())
                {
                    clearDrawing();
                    updateLabelFields(getLastClick(), e.Location);

                    //draw the new selection in progress
                    Graphics gr = pictureBox1.CreateGraphics();
                    drawBoxCustom(getLastClick(), e.Location, gr, selectionList.Count, theHighlightedPenIs);
                    gr.Dispose();

                    //draw the rest of the selections
                    drawBoxes();
                }
            }

        }

        private void mouseMoveEyedropAction(MouseEventArgs e)
        {
            //TODO
        }

        /****************************************************************************
         * FUNCTION mouseUpSelectAction
         * 
         * Performs designated actions if mouseUp event occurs and the select tool is 
         * currently being used.
         * 
         * parameters:
         * mouseEventArg (mouse up event)
         * 
         * returns:
         * void
         * 
         ****************************************************************************/
        private void mouseUpSelectAction(MouseEventArgs e)
        {
            //update state
            setMouseState(false);
            pictureBox1.Cursor = Cursors.Arrow;
            mouseUpPoint.X = e.X;
            mouseUpPoint.Y = e.Y;
                 //if the selection has any area selected..
            if ((getLastClick().X != e.X) && (getLastClick().Y != e.Y))
            {
                addSelection(getLastClick(), e.Location); //selection of focus is set within this to the latest                              
            }
            else 
            {   //click was trying to select a particular box
                determineSelection(e.Location);
            }

            if(selectionOfFocus != null) updateLabelFields(selectionOfFocus.startPoint, selectionOfFocus.endPoint); //x&y data
            updateOtherFields();//selection label, color, checkbox, threshold text...

            //draw final selection
            drawBoxes();
        }

        private void mouseUpEyedropAction(MouseEventArgs e)
        {
            
        }

        private void determineSelection(Point clickLoc)
        {
            //find the clicked selection
            int indexOfClickedSelection = findClickedSelection(clickLoc);

            //if a selection was found, set it to be focus.
            if (indexOfClickedSelection >= 0) selectionOfFocus = selectionList[indexOfClickedSelection];

        }

        /*
         * Function: findClickedSelection( Point p ) 
         * 
         * Returns -1 if no selection was found, otherwise, returns the index of the clicked selection
         * 
         */
        private int findClickedSelection(Point p)
        {

            //for each selection
            //determine if selection was inside this hitbox

            int index = -1;

            for (int i = 0; i < selectionList.Count; i++)
            {
                if (locationIsInRectangle(selectionList[i].startPoint, selectionList[i].endPoint, p)) //defined by pt1, pt2, 
                {
                    index = i;
                }
            }

            return index;
        }
        private void toggleFocusOCRState()
        {
            selectionOfFocus.isOCR = !selectionOfFocus.isOCR;
        }


#endregion

#region List Editing
        private void addSelection(Point firstPoint, Point secondPoint, Color color = new Color())
        {
            //generate new selection object
            selectedArea selection = new selectedArea(
                                                        firstPoint,             //the mouseDown location
                                                        secondPoint,            //the associated mouseUp location
                                                        selectionList.Count,    //index of the selection
                                                        color,                  //the associated color of the selection
                                                        false,                   //defaults to NON-OCR selection
                                                        30
                                                                                );
            selectionList.Add(selection);
            setButtonActiveState(this.done_button, true);
            selectionOfFocus = selectionList[selectionList.Count - 1]; //set selection to highest
            updateLabelFields(selection.startPoint, selection.endPoint);
        }

        private void removeLastSelection()
        {
            if (selectionList.Count > 0) //are there any objects left?
            {                             //...yes, at least 1
                //remove the last added selection object
                selectionList.RemoveAt(selectionList.Count-1);                

                if (selectionList.Count > 0) //still any objects there?
                {                             //yes, populate the label fields with the top of the stack's data
                    updateLabelFields(selectionList[selectionList.Count - 1].startPoint,
                                 selectionList[selectionList.Count - 1].endPoint);
                    selectionOfFocus = selectionList[selectionList.Count - 1];
                }
                else
                {   //...there are none left on the list
                    //zero the fields
                    setButtonActiveState(this.done_button, false);
                    updateLabelFields(new Point(0, 0), new Point(0, 0));
                    selectionOfFocus = null;
                    this.threshold_text.Text = "";
                    this.ocr_checkbox.CheckState = CheckState.Unchecked;
                }
            }

        }

        private void setColorOfLastSelection(Color color)
        {
            if (selectionOfFocus != null) selectionOfFocus.chosenColor = color;
            /*
             * You could keep it simple and use the native colour translator:
             *
             * Color red = ColorTranslator.FromHtml("#FF0000");
             * string redHex = ColorTranslator.ToHtml(red);
             *
             * Then break the three colour pairs into decimal:
             *
             * int decValue = int.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
             *
             *
             */
        }

#endregion

#region Drawing
        private void drawBoxes()
        {
            
            Pen penOfChoice;
            Graphics g = pictureBox1.CreateGraphics();
            //draw all of the saved selections...
            for (int i = 0; i < selectionList.Count ; i++)
            {
                //which pen is used to draw each box?
                penOfChoice = (selectionList[i].isOCR) ? theTextPenIs : theWatcherPenIs;
                if (selectionOfFocus != null)
                {    
                    if (selectionOfFocus.myIndex == selectionList[i].myIndex) penOfChoice = theHighlightedPenIs;
                }
                
                drawBoxCustom(selectionList[i].startPoint, selectionList[i].endPoint, g, selectionList[i].myIndex, penOfChoice);
                
            }
            g.Dispose();
        }

        private void clearDrawing()
        {

            //clear the current selection
            pictureBox1.Invalidate();  
            
            //g.Clear(System.Drawing.Color.Transparent); doesnt work
        }

        private void drawBoxCustom(Point firstPoint, Point secondPoint, Graphics graphicObj, int index, Pen penOfChoice)
        {

            graphicObj.DrawLine(penOfChoice, firstPoint, generatedHorizontalPoint(firstPoint, secondPoint));   //mouseDown -> mouseDown+dX
            graphicObj.DrawLine(penOfChoice, firstPoint, generatedVerticalPoint(firstPoint, secondPoint));     //mouseDown -> mouseDown+dY
            graphicObj.DrawLine(penOfChoice, generatedHorizontalPoint(firstPoint, secondPoint), secondPoint);  //mouseDown+dX -> mouseUp
            graphicObj.DrawLine(penOfChoice, generatedVerticalPoint(firstPoint, secondPoint), secondPoint);    //mouseDown+dY -> mouseUp
       
            
            //draw associated index value at the top left corner of the rectangle
            graphicObj.DrawString(""+(index+1), drawFont, theBrush, whereToDrawNumber(firstPoint, secondPoint));
        }

        private Point whereToDrawNumber(Point firstPoint, Point secondPoint)
        {
            Point drawHere;
            int height = getHeight(firstPoint.Y,secondPoint.Y);
            int width  = getWidth(firstPoint.X, secondPoint.X);

            int letterWidth = (int)drawFont.Size; //to correct for exactly where the whatever draws shit actually draws the letter (perfectly centers it)

            if ( height < 0 )
            {
                drawHere = new Point(firstPoint.X + (width / 2) - letterWidth / 2, firstPoint.Y);       //sit on original click locatino
            }
            else
            {
                drawHere = new Point(firstPoint.X + (width / 2) - letterWidth/2, firstPoint.Y+(height)); //follow Y axis
            }

            return drawHere;

             /* is the selection inverted? if so, paint index with cursor 
                  ((getWidth(firstPoint.X, secondPoint.X) > 0  // mouse moved right
                 && getHeight(firstPoint.Y, secondPoint.Y) > 0 // mouse moved down
                    ) ? firstPoint: secondPoint) */
        }

        private void picturebox_paint(object sender, PaintEventArgs e)
        {
            drawBoxes();
        }

#endregion

#region Helper Functions
        /*****************************************
         * 
         * HELPER FUNCTIONS BELOW
         * 
         *****************************************/
        private void setMouseState(bool p)
        {
            mouseIsDown_GLOBAL = p;
        }

        private bool getMouseState()
        {
            return this.mouseIsDown_GLOBAL;
        }
        private int radioState()
        {
            if (this.eyedrop_tool_radio.Checked)
            {
                return (int)tool_enum.EYEDROP_TOOL;
            }
            else if (this.select_tool_radio.Checked)
            {
                return (int)tool_enum.SELECT_TOOL;
            }

            return 0; //select tool
        }

        private Color collectColor(Point e)
        {

            Graphics g = pictureBox1.CreateGraphics();

            IntPtr myDC = g.GetHdc();

            Color c = ColorTranslator.FromWin32(GetPixel(myDC, e.X, e.Y));

            g.ReleaseHdc(myDC);
            g.Dispose();

            return c;
        }

        private void recordClick(Point point, int p1, int p2)
        {
            point.X = p1;
            point.Y = p2;          
        }
        /*
        *Function getLastClick
        *
        * Returns the last position in which mousedown was activated
        * on the picturebox frame.
        * 
        * Returns:
        *  Point 
        */

        private Point getLastClick()
        {

            return this.mouseDownPoint;
        }

        private Point generatedVerticalPoint(Point firstPoint, Point secondPoint)
        {
            Point vertPoint = new Point();

            vertPoint.X = firstPoint.X;
            vertPoint.Y = firstPoint.Y + getHeight(firstPoint.Y, secondPoint.Y);

            return vertPoint;

        }

        private Point generatedHorizontalPoint(Point firstPoint, Point secondPoint)
        {
            Point horizontalPoint = new Point();

            horizontalPoint.X = firstPoint.X + getWidth(firstPoint.X, secondPoint.X);
            horizontalPoint.Y = firstPoint.Y;

            return horizontalPoint;

        }
        private int getHeight(int firstLocation, int secondLocation)
        {
            return secondLocation - firstLocation;
        }

        private int getWidth(int firstLocation, int secondLocation)
        {
            return secondLocation - firstLocation;
        }

        private void setButtonActiveState(Button button, bool p)
        {
            button.Enabled = p;
        }

        /*
         * FUNCTION roundToNearestInt
         * 
         * Rounds an int to the value that it is nearest to...why this doesn't
         * already exist is beyond me. 
         * 
         * parameters:
         * 
         * int value to be rounded
         * 
         * returns:
         * 
         * rounded int (integer) value
         * 
         * concept:
         * round up and store it. At the same time, add .5 to 
         * the original value. if the values match, then the 
         * original value had a decimal that was >.5, and 
         * should be rounded up.
         */
        private int roundToNearestInt(double p)
        {
            int roundedUpInt, addedHalfInt;

            //round up with one
            decimal roundedUpVal = Math.Ceiling((decimal)p);
            double half = 0.5;

            //add half to the original
            double addedHalfVal = p + half;

            roundedUpInt = (int)roundedUpVal;
            addedHalfInt = (int)Math.Floor(addedHalfVal);

            //do they match?
            if (roundedUpInt == addedHalfInt)
            {
                return roundedUpInt; //yes
            }
            else
            {
                return addedHalfInt; //no
            }

        }

        
        private void transposeDataPoints()
        {

            int pictureBoxWidth, pictureBoxHeight, imageWidth, imageHeight;
            double pctX, pctY;
            Point thePoint, transPoint;

            pictureBoxWidth = pictureBox1.Size.Width;
            pictureBoxHeight = pictureBox1.Size.Height;
            imageWidth = img.Width;
            imageHeight = img.Height;

            for (int i = 0; i < selectionList.Count; i++)
            {
                //get the starting point
                thePoint = selectionList[i].startPoint;
                //convert the X and Y values to percentages
                pctX = (((double)thePoint.X) / ((double)pictureBoxWidth));
                pctY = (((double)thePoint.Y) / ((double)pictureBoxHeight));

                transPoint = new Point(roundToNearestInt(pctX * imageWidth), roundToNearestInt(pctY * imageHeight));

                selectionList[i].translatedStartPoint = transPoint;

                thePoint = selectionList[i].endPoint;

                pctX = (((double)thePoint.X) / ((double)pictureBoxWidth));
                pctY = (((double)thePoint.Y) / ((double)pictureBoxHeight));

                transPoint = new Point(roundToNearestInt(pctX * imageWidth), roundToNearestInt(pctY * imageHeight));

                selectionList[i].translatedEndPoint = transPoint;

            }

        }
       
        /**************************************************************************************
         * FUNCTION transposeDataPoints()                                                     *
         *                                                                                    *
         * Converts the gathered data selections into pixel locations in the chosen bitmap    *
         * image. This step is necessary due to the fluctuating size of the viewed bitmap.    *
         * The selected pixel values therefore will not necessarily correspond to the pixel   *
         * locations until after this function is run.                                        *
         *                                                                                    *
         * returns:                                                                           *
         *                                                                                    *
         * paramters:                                                                         *
         *                                                                                    *
         * globals:                                                                           *
         *                                                                                    *
         *      List <selectedArea> selectedList                                              *
         *                                                                                    *
         * Functionality:                                                                     *
         * -----------------                                                                  *
         * get picturebox dimensions                                                          *
         * get actual image dimensions                                                        *
         * for each selection                                                                 *
         * get data points                                                                    *
         *                                                                                    *
         * change picturebox dimensions into percentages                                      *
         * mutiply actual dimensions with percentages, roud up and down accordingly,          *
         * store data                                                                         *
         *                                                                                    *
         **************************************************************************************/
        /* private void transposeDataPoints()
        {
            foreach (selectedArea item in selectionList) 
            {
                item.translatedStartPoint = transposeDataPoint(pictureBox1.Size, img.Size, item.startPoint);
                item.translatedEndPoint   = transposeDataPoint(pictureBox1.Size, img.Size, item.endPoint);
            }
        }
        */
        private Point transposeDataPoint(Size prevSize, Size newSize, Point pointToTranspose)
        {

            double pctX;
            double pctY;

            //convert the X and Y values to percentages
            pctX = (((double)pointToTranspose.X) / ((double)prevSize.Width));
            pctY = (((double)pointToTranspose.Y) / ((double)prevSize.Height));

            //scale the new size to the previous X Y values
            return new Point(roundToNearestInt(newSize.Width * pctX), roundToNearestInt(newSize.Height * pctY));
            
        }

        private Point getCenterOfRectangle(Point firstPoint, Point secondPoint)
        {
            int width  = secondPoint.X - firstPoint.X;
            int height = secondPoint.Y - firstPoint.Y;

            return new Point((firstPoint.X) + (width / 2), (firstPoint.Y) + (height / 2));

            /* use below to determine quadrants if necesary
            if (width > 0 && height < 0) 
            {                           //quadrant 1
                
            }
            else if (width < 0 && height < 0)
            {                           //quadrant 2

            }
            else if (width > 0 && height < 0)
            {                           //quadrant 3

            }
            else
            {                           //quadrant 4

            }*/

        }
        /*
         * point 1 and point 2 are the bounding regions of a rectangle,
         * this function returns true of location is within the rectangle
         * 
         */
        private bool locationIsInRectangle(Point point1, Point point2, Point location)
        {
            //if given click x is between start and end X, and given click y is between start and end Y...
            return ((  //x is within bounds
                     (location.X < Math.Max(point1.X, point2.X))
                   && (location.X > Math.Min(point1.X, point2.X)))
               && //AND Y is within sbounds
                  ((location.Y < Math.Max(point1.Y, point2.Y))
                   && (location.Y > Math.Min(point1.Y, point2.Y))));
        }


        private void renderPictureBox()
        {
            Size storedSize, newSize;
            if (ratio_lock_checkbox.CheckState == CheckState.Checked)
            {
                storedSize = pictureBox1.Size;
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                newSize = determineImageSizeInPicturebox(pictureBox1.Size, img.Size);
            }
            else
            {
                storedSize = determineImageSizeInPicturebox(pictureBox1.Size, img.Size);
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                newSize = pictureBox1.Size;
            }
            refactorDataPoints(storedSize, newSize);
        }

        private Size determineImageSizeInPicturebox(Size pictureBox, Size img)
        {
            double aspectRatio;
            int pictureWidth;

            aspectRatio = (double)img.Width / (double)img.Height;
            pictureWidth = roundToNearestInt(pictureBox.Height * aspectRatio);

            return new Size(pictureWidth, pictureBox.Height);
        }

        private void refactorDataPoints(Size storedSize, Size newSize)
        {
            if (selectionList.Count > 0)
            {

                foreach (selectedArea item in selectionList)
                {
                    item.startPoint = transposeDataPoint(storedSize, newSize, item.startPoint);
                    item.endPoint = transposeDataPoint(storedSize, newSize, item.endPoint);
                }

                if (selectionOfFocus != null) 
                {
                    updateLabelFields(selectionOfFocus.startPoint, selectionOfFocus.endPoint); //x&y data
                    updateOtherFields();//selection label, color, checkbox, threshold text...
                } 
                

            }
        }

#endregion

#region Label Functions


        private void updateCursorPosition(Point e)
        {
            string cursor = "" + e.X + ", " + e.Y;

            this.cursor_position_field.Text = cursor;
        }
        private void updatePointField(Point firstPoint, Point secondPoint)
        {
            Point center = getCenterOfRectangle(firstPoint, secondPoint);

            this.xy_select_text.Text = "" + center.X + ", " + center.Y;
            
        }

        private void updateWHField(Point firstPoint, Point secondPoint)
        {
            this.wh_select_text.Text = "" + System.Math.Abs(secondPoint.X - firstPoint.X) + ", " +
                                            System.Math.Abs(secondPoint.Y - firstPoint.Y);
        }

        private void updateLabelFields(Point firstPoint, Point secondPoint)
        {
            if (firstPoint != null && secondPoint != null) 
            {
                updatePointField(firstPoint, secondPoint);
                updateWHField(firstPoint, secondPoint);
            }
        }

        private void updateSelectionLabel()
        {
            if (selectionList.Count == 0) this.selection_count_label.Text = (selectionList.Count) + " selections";
            else this.selection_count_label.Text = (selectionOfFocus.myIndex+1) +" of " + (selectionList.Count) + " selections";
        }

        private void updateColorField()
        {

            if (selectionList.Count == 0)
            {
                this.color_select_text.Text = "Empty";
                return;
            }

            this.color_select_text.Text = selectionOfFocus.chosenColor.ToString();           
                
        }
        private void updateOCRCheckbox()
        {
            if(selectionOfFocus != null)
            {
                if (selectionOfFocus.isOCR) this.ocr_checkbox.CheckState = CheckState.Checked;
                else this.ocr_checkbox.CheckState = CheckState.Unchecked;
            }
        }
        private void updateColorThreshold()
        {
            if(selectionOfFocus != null) this.threshold_text.Text = selectionOfFocus.colorThreshold+"";
        }

        private void updateOtherFields()
        {
            updateSelectionLabel();
            updateColorField();
            updateOCRCheckbox();
            updateColorThreshold();
        }
#endregion


        #region Loading Functions


        private void loadObjectsFromXML()
        {
            //deserialize the saved objects in the text file.
            using (var fDialog = new System.Windows.Forms.OpenFileDialog())
            {
                fDialog.Filter = "Image Config Files (*.xml)|*.xml";
                fDialog.Title = "Select the object XML";

                if (fDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) { }

                //if you can even call it sanitation...
                if (fDialog.FileName.Length > 0) matchData = loadMatch(fDialog.FileName);
            }

            if(matchData.teams.Count > 0) populateMatchData(matchData);
        }

        private void populateMatchData(Match matchForPopulating)
        {
            this.data_listview.Groups.Clear();//clear previous list
            Application.DoEvents();//right now

            /*create both images*/
            Bitmap direImg = new Bitmap(3, 3, PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(direImg);
            g.FillRectangle(Brushes.Red, new Rectangle(0, 0, 3, 3));
            Bitmap radImg = new Bitmap(3, 3, PixelFormat.Format24bppRgb);
            g = Graphics.FromImage(radImg);
            g.FillRectangle(Brushes.Green, new Rectangle(0, 0, 3, 3));
            /**/
            g.Dispose();//dispose graphocs
                       
            ListViewGroup direGroup = new ListViewGroup("Team Dire");
            ListViewGroup radGroup = new ListViewGroup("Team Radiant");
            ListViewGroup towerGroup = new ListViewGroup("Towers");
            ListViewGroup[] groups = { direGroup, radGroup, towerGroup };

            ImageList imgList = new ImageList();
            imgList.Images.Add("direImage", direImg);
            imgList.Images.Add("radImage", radImg);
            this.data_listview.SmallImageList = imgList; 
            //direImg.Dispose();
            //radImg.Dispose();

            this.data_listview.Groups.AddRange(groups);

            string[] playerDat = 
            {
                "",
                " Level",
                 " Gold",
                 " G/m",
                 " XP/m"
            };
            string [] teamName = 
            {
                "Dire ",
                "Radiant "
            };
            string[] readTeamNames =
            {                
                "Dire Team Name",
                "Radiant Team Name"

            };
            string[] imageLists=
            {
                "direImage",
                "radImage"
            };

            for (int i = 0; i < global_constants.TEAMS_IN_MATCH;i++ ) 
            {
                int tselectionIndex = matchForPopulating.teams[i].attributes[0].index;
                ListViewItem teamItem = new ListViewItem(new[] {(matchDataWasLoadedInFlag) ? matchForPopulating.teams[i].attributes[0].text : readTeamNames[i],
                                                                (matchDataWasLoadedInFlag) ?                                    //if data was loaded
                                                                   ( tselectionIndex< 0)? "-" : // does its index exist? - no
                                                                        (tselectionIndex+1)+""     //yes
                                                                   : "-" }, 0, groups[i]);                                      //data was not loaded
                teamItem.ImageKey = imageLists[i];
                this.data_listview.Items.Add(teamItem);

                for (int j = 0; j < global_constants.PLAYERS_ON_TEAM; j++)
                {
                    for (int k = 0; k < global_constants.DATA_IN_PLAYER;k++ ) 
                    {
                        int selectionIndex = matchForPopulating.teams[i].players[j].attributes[k].index;
                        ListViewItem nextItem = new ListViewItem(new[] { teamName[i] + "Player " + (j + 1) + playerDat[k], 
                                 (matchDataWasLoadedInFlag) ?                                                 //if data was loaded
                                     (selectionIndex < 0)? "-" :  // does its index exist? - no
                                           (selectionIndex+1)+"" :    //yes
                                     "-" }, 0, groups[i]);                                                    //data was not loaded
                        if ((j & 1) == 0) nextItem.ForeColor = Color.SaddleBrown;//alternating colors by player
                        nextItem.ImageKey = imageLists[i];
                        this.data_listview.Items.Add(nextItem);
                    }
                }
            }
            for (int i = 0; i < global_constants.TEAMS_IN_MATCH; i++)
            {
                                for (int j = 0; j < global_constants.TOWERS_PER_TEAM; j++)
                {
                    ListViewItem nextItem;
                    int selectionIndex = matchForPopulating.teams[i].towers[j].status.index;
                    if (j < 9)
                    {
                        nextItem = new ListViewItem(new[] { teamName[i] + "Tower", 
                            (matchDataWasLoadedInFlag) ?                                        //if data was loaded
                                ( selectionIndex < 0)? "-":                                     // does its index exist? - no
                                     (selectionIndex+1)+"" :                                    //yes
                                "-" }, 0, groups[2]);                                           //data was not loaded
                    }
                    else if (j == 9)
                    {
                        nextItem = new ListViewItem(new[] { teamName[i] + "Barracks Tower", 
                            (matchDataWasLoadedInFlag) ?                                        //if data was loaded
                                ( selectionIndex < 0)? "-":                                     // does its index exist? - no
                                     (selectionIndex+1)+"" :                                    //yes
                                "-" }, 0, groups[2]);                                           //data was not loaded
                    }
                    else
                    {
                        nextItem = new ListViewItem(new[] { teamName[i] + "Ancient Tower", 
                            (matchDataWasLoadedInFlag) ?                                        //if data was loaded
                                ( selectionIndex < 0)? "-":                                     // does its index exist - no
                                     (selectionIndex+1)+"" :                                    //yes
                                "-" }, 0, groups[2]);                                           //data was not loaded
                    }                                                                                                                 
                    if ((i & 1) == 0) nextItem.ForeColor = Color.SaddleBrown;//alternating colors by player                          
                    nextItem.ImageKey = imageLists[i];
                    this.data_listview.Items.Add(nextItem);
                }
            }


        }

        private Match loadMatch(string loadFileLocation)
        {
            Match tempMatch = new Match();
            XmlSerializer ser = new XmlSerializer(typeof(Match));
            try
            {
                Stream stream = new FileStream(loadFileLocation, FileMode.Open, FileAccess.Read, FileShare.Read);

                tempMatch = (Match)ser.Deserialize(stream);
                stream.Close();
            }
            catch// (Exception e) 
            {
                MessageBox.Show("File not valid.");
            }

            return tempMatch;
        }


        #endregion


        #region ListView Events


        private void item_hover_event(object sender, ListViewItemMouseHoverEventArgs e)
        {           
            //Console.WriteLine("HOVER EVENT");
        }

        private void item_drag_Event(object sender, ItemDragEventArgs e)
        {

            if (this.data_listview.SelectedIndices.Count > 1) //multiple items..
            {
                foreach (ListViewItem item in this.data_listview.SelectedItems)
                {

                    DoDragDrop(item, DragDropEffects.Move);
                }
            }
            else 
            {
                DoDragDrop(e.Item, DragDropEffects.Move);
            }
        }

        private void item_activate_Event(object sender, EventArgs e)
        {
            //Console.WriteLine("ACTIVATE EVENT");
        }

        private void item_double_click_Event(object sender, EventArgs e)
        {
            //Console.WriteLine("DOUBLE CLICK EVENT");
            foreach (ListViewItem itemClicked in this.data_listview.SelectedItems)
            {
                bindItemToSelection(itemClicked, -1);
            }          
        }

        #endregion

        private void picturebox_dragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                e.Effect = DragDropEffects.Move;
            }

        }

        private void picturebox_dragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(ListViewItem)))
            {
                processDroppedObject(e);
                //this.data_listview.Items.Remove(item);
            }
            else if(e.Data.GetDataPresent(typeof(ListViewItem.ListViewSubItemCollection)))
            {
                ListViewItem.ListViewSubItemCollection itemGroup = (ListViewItem.ListViewSubItemCollection)e.Data.GetData(typeof(ListViewItem.ListViewSubItemCollection));
                foreach (ListViewItem item in itemGroup) 
                {
                    processDroppedObject(e);
                }
            }
        }

        private void processDroppedObject(DragEventArgs e)
        {
            ListViewItem item = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
            Point screenControlPoint = new Point(e.X, e.Y);
            Point droppedPoint       = this.pictureBox1.PointToClient(screenControlPoint);
            int indexOfClickedSelection = findClickedSelection(droppedPoint);
            Point psdgs = pictureBox1.Location;
            if (indexOfClickedSelection >= 0) 
            {
                bindItemToSelection(item, indexOfClickedSelection);
            } 

            
            Console.WriteLine(item.Text);
        }

        //a shitty function for this extremely specific application
        private void bindItemToSelection(ListViewItem item, int indexOfClickedSelection)
        {
            string itemText = item.SubItems[0].Text;
            string[] parts = itemText.Split(' ');
            int itemIndex = item.Index;

            //set the text to the matched selection
            if (indexOfClickedSelection >= 0) item.SubItems[1].Text = "" + (indexOfClickedSelection + 1);
            else item.SubItems[1].Text = "-";


            //identify the item
            //indicies for the items is known, so just use them.
            //eliminate team names
            if (itemIndex == 0 || itemIndex == (((global_constants.TEAMS_IN_MATCH * 
                     global_constants.PLAYERS_ON_TEAM * global_constants.DATA_IN_PLAYER) / 2)+1) )
            {
                if (itemIndex > 0)
                {//its the radiant team name
                    matchData.teams[1].attributes[0].index = indexOfClickedSelection;
                    return;
                }
                matchData.teams[0].attributes[0].index = indexOfClickedSelection; //dire team name
                return;
            }

            //separate by teams
            if (itemIndex <= (global_constants.PLAYERS_ON_TEAM * global_constants.DATA_IN_PLAYER))
            {//lower half is dire

                int indexer = ((int)Math.Ceiling((double)itemIndex / (double)global_constants.DATA_IN_PLAYER))-1; // which player 1-5?

                int attributeIndex = (itemIndex-1) % global_constants.DATA_IN_PLAYER;//from 0-24, cycling around 5's

                matchData.teams[0].players[indexer].attributes[attributeIndex].index = indexOfClickedSelection;

                return;
            }
            else if (itemIndex <= ((global_constants.TEAMS_IN_MATCH * 
                global_constants.PLAYERS_ON_TEAM * global_constants.DATA_IN_PLAYER) + 1)) //+1 for the team name..so ugly
            {
                itemIndex -= global_constants.PLAYERS_ON_TEAM * global_constants.DATA_IN_PLAYER + global_constants.TEAMS_IN_MATCH/2; //normalize data
                int indexer = ((int)Math.Ceiling((double)itemIndex / (double)global_constants.DATA_IN_PLAYER))-1;
                int attributeIndex = (itemIndex-1) % global_constants.DATA_IN_PLAYER;

                matchData.teams[1].players[indexer].attributes[attributeIndex].index = indexOfClickedSelection;

                return;
            }

            //it's a tower do something
            if (itemIndex < (global_constants.TEAMS_IN_MATCH * global_constants.PLAYERS_ON_TEAM * global_constants.DATA_IN_PLAYER) +
                global_constants.TEAMS_IN_MATCH/*for their names*/ + global_constants.TOWERS_PER_TEAM)
            {
                //it's a dire tower
                //normalize it
                int normalizer = ((global_constants.TEAMS_IN_MATCH * global_constants.PLAYERS_ON_TEAM * global_constants.DATA_IN_PLAYER) + 
                    global_constants.TEAMS_IN_MATCH/*for their names*/);
                int normalIndex = itemIndex - normalizer;
                matchData.teams[0].towers[normalIndex].status.index = indexOfClickedSelection;
                return;

            }
            else
            {
                //rad tower
                //normalize it
                int normalIndex = itemIndex - ((global_constants.TEAMS_IN_MATCH * global_constants.PLAYERS_ON_TEAM * 
                    global_constants.DATA_IN_PLAYER) + global_constants.TEAMS_IN_MATCH/*for their names*/ + global_constants.TOWERS_PER_TEAM);
                matchData.teams[1].towers[normalIndex].status.index = indexOfClickedSelection;
            }
        }

        private void form_closed_Event(object sender, FormClosedEventArgs e)
        {         
            if( selectionList.Count > 0 ) transposeDataPoints();
            saveSize();
            //img.Dispose();           
        }

        private void clearSelectedItemBindingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem itemClicked in this.data_listview.SelectedItems)
            {
                bindItemToSelection(itemClicked, -1);//for all those selected, set their index value to -1 (do not process)
            }    
        }
    }
}

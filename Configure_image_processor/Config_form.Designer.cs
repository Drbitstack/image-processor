﻿namespace Configure_image_processor
{
    partial class Config_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.select_image_button = new System.Windows.Forms.Button();
            this.working_image_text = new System.Windows.Forms.TextBox();
            this.identify_section_button = new System.Windows.Forms.Button();
            this.save_button = new System.Windows.Forms.Button();
            this.go_button = new System.Windows.Forms.Button();
            this.stop_button = new System.Windows.Forms.Button();
            this.run_every_field = new System.Windows.Forms.TextBox();
            this.static_text_1 = new System.Windows.Forms.Label();
            this.static_text_2 = new System.Windows.Forms.Label();
            this.load_button = new System.Windows.Forms.Button();
            this.runtime_label = new System.Windows.Forms.Label();
            this.progress_bar = new System.Windows.Forms.ProgressBar();
            this.status_label = new System.Windows.Forms.Label();
            this.static_label_status = new System.Windows.Forms.Label();
            this.static_label_action_count = new System.Windows.Forms.Label();
            this.action_count_label = new System.Windows.Forms.Label();
            this.clear_events_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // select_image_button
            // 
            this.select_image_button.Location = new System.Drawing.Point(12, 12);
            this.select_image_button.Name = "select_image_button";
            this.select_image_button.Size = new System.Drawing.Size(132, 24);
            this.select_image_button.TabIndex = 0;
            this.select_image_button.Text = "Select Working Image";
            this.select_image_button.UseVisualStyleBackColor = true;
            this.select_image_button.Click += new System.EventHandler(this.select_image_button_Click);
            // 
            // working_image_text
            // 
            this.working_image_text.Location = new System.Drawing.Point(13, 42);
            this.working_image_text.Name = "working_image_text";
            this.working_image_text.Size = new System.Drawing.Size(250, 20);
            this.working_image_text.TabIndex = 3;
            this.working_image_text.TextChanged += new System.EventHandler(this.working_image_text_TextChanged);
            // 
            // identify_section_button
            // 
            this.identify_section_button.Location = new System.Drawing.Point(12, 191);
            this.identify_section_button.Name = "identify_section_button";
            this.identify_section_button.Size = new System.Drawing.Size(119, 25);
            this.identify_section_button.TabIndex = 4;
            this.identify_section_button.Text = "Identify Sections";
            this.identify_section_button.UseVisualStyleBackColor = true;
            this.identify_section_button.Click += new System.EventHandler(this.identify_section_button_Click);
            // 
            // save_button
            // 
            this.save_button.Location = new System.Drawing.Point(235, 194);
            this.save_button.Name = "save_button";
            this.save_button.Size = new System.Drawing.Size(61, 22);
            this.save_button.TabIndex = 7;
            this.save_button.Text = "Save";
            this.save_button.UseVisualStyleBackColor = true;
            this.save_button.Click += new System.EventHandler(this.save_button_Click);
            // 
            // go_button
            // 
            this.go_button.Location = new System.Drawing.Point(12, 153);
            this.go_button.Name = "go_button";
            this.go_button.Size = new System.Drawing.Size(36, 23);
            this.go_button.TabIndex = 9;
            this.go_button.Text = "Go";
            this.go_button.UseVisualStyleBackColor = true;
            this.go_button.Click += new System.EventHandler(this.go_button_Click);
            // 
            // stop_button
            // 
            this.stop_button.Location = new System.Drawing.Point(56, 153);
            this.stop_button.Name = "stop_button";
            this.stop_button.Size = new System.Drawing.Size(75, 23);
            this.stop_button.TabIndex = 10;
            this.stop_button.Text = "Stop";
            this.stop_button.UseVisualStyleBackColor = true;
            this.stop_button.Click += new System.EventHandler(this.stop_button_Click);
            // 
            // run_every_field
            // 
            this.run_every_field.Location = new System.Drawing.Point(72, 127);
            this.run_every_field.Name = "run_every_field";
            this.run_every_field.Size = new System.Drawing.Size(57, 20);
            this.run_every_field.TabIndex = 11;
            // 
            // static_text_1
            // 
            this.static_text_1.AutoSize = true;
            this.static_text_1.Location = new System.Drawing.Point(12, 130);
            this.static_text_1.Name = "static_text_1";
            this.static_text_1.Size = new System.Drawing.Size(56, 13);
            this.static_text_1.TabIndex = 12;
            this.static_text_1.Text = "Run every";
            // 
            // static_text_2
            // 
            this.static_text_2.AutoSize = true;
            this.static_text_2.Location = new System.Drawing.Point(135, 130);
            this.static_text_2.Name = "static_text_2";
            this.static_text_2.Size = new System.Drawing.Size(23, 13);
            this.static_text_2.TabIndex = 13;
            this.static_text_2.Text = "ms.";
            // 
            // load_button
            // 
            this.load_button.Location = new System.Drawing.Point(168, 194);
            this.load_button.Name = "load_button";
            this.load_button.Size = new System.Drawing.Size(61, 23);
            this.load_button.TabIndex = 14;
            this.load_button.Text = "Load";
            this.load_button.UseVisualStyleBackColor = true;
            this.load_button.Click += new System.EventHandler(this.load_button_Click);
            // 
            // runtime_label
            // 
            this.runtime_label.AutoSize = true;
            this.runtime_label.Location = new System.Drawing.Point(247, 163);
            this.runtime_label.Name = "runtime_label";
            this.runtime_label.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.runtime_label.Size = new System.Drawing.Size(49, 13);
            this.runtime_label.TabIndex = 15;
            this.runtime_label.Text = "00:00:00";
            // 
            // progress_bar
            // 
            this.progress_bar.Location = new System.Drawing.Point(232, 178);
            this.progress_bar.Name = "progress_bar";
            this.progress_bar.Size = new System.Drawing.Size(63, 10);
            this.progress_bar.TabIndex = 16;
            // 
            // status_label
            // 
            this.status_label.AutoSize = true;
            this.status_label.Location = new System.Drawing.Point(174, 163);
            this.status_label.Name = "status_label";
            this.status_label.Size = new System.Drawing.Size(47, 13);
            this.status_label.TabIndex = 17;
            this.status_label.Text = "Stopped";
            // 
            // static_label_status
            // 
            this.static_label_status.AutoSize = true;
            this.static_label_status.Location = new System.Drawing.Point(135, 163);
            this.static_label_status.Name = "static_label_status";
            this.static_label_status.Size = new System.Drawing.Size(40, 13);
            this.static_label_status.TabIndex = 18;
            this.static_label_status.Text = "Status:";
            // 
            // static_label_action_count
            // 
            this.static_label_action_count.AutoSize = true;
            this.static_label_action_count.Location = new System.Drawing.Point(165, 130);
            this.static_label_action_count.Name = "static_label_action_count";
            this.static_label_action_count.Size = new System.Drawing.Size(67, 13);
            this.static_label_action_count.TabIndex = 19;
            this.static_label_action_count.Text = "Event Ticks:";
            // 
            // action_count_label
            // 
            this.action_count_label.AutoSize = true;
            this.action_count_label.Location = new System.Drawing.Point(247, 130);
            this.action_count_label.Name = "action_count_label";
            this.action_count_label.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.action_count_label.Size = new System.Drawing.Size(13, 13);
            this.action_count_label.TabIndex = 20;
            this.action_count_label.Text = "0";
            // 
            // clear_events_button
            // 
            this.clear_events_button.BackColor = System.Drawing.Color.White;
            this.clear_events_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear_events_button.Location = new System.Drawing.Point(281, 130);
            this.clear_events_button.Margin = new System.Windows.Forms.Padding(0);
            this.clear_events_button.Name = "clear_events_button";
            this.clear_events_button.Size = new System.Drawing.Size(18, 16);
            this.clear_events_button.TabIndex = 21;
            this.clear_events_button.Text = "C";
            this.clear_events_button.UseVisualStyleBackColor = false;
            this.clear_events_button.Click += new System.EventHandler(this.clear_events_button_Click);
            // 
            // Config_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 228);
            this.Controls.Add(this.clear_events_button);
            this.Controls.Add(this.action_count_label);
            this.Controls.Add(this.static_label_action_count);
            this.Controls.Add(this.static_label_status);
            this.Controls.Add(this.status_label);
            this.Controls.Add(this.progress_bar);
            this.Controls.Add(this.runtime_label);
            this.Controls.Add(this.load_button);
            this.Controls.Add(this.static_text_2);
            this.Controls.Add(this.static_text_1);
            this.Controls.Add(this.run_every_field);
            this.Controls.Add(this.stop_button);
            this.Controls.Add(this.go_button);
            this.Controls.Add(this.save_button);
            this.Controls.Add(this.identify_section_button);
            this.Controls.Add(this.working_image_text);
            this.Controls.Add(this.select_image_button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Config_form";
            this.Text = "Configure Image Processor";
            this.Load += new System.EventHandler(this.Config_form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.Button select_image_button;
        private System.Windows.Forms.TextBox working_image_text;
        private System.Windows.Forms.Button identify_section_button;
        private System.Windows.Forms.Button save_button;
        private System.Windows.Forms.Button go_button;
        private System.Windows.Forms.Button stop_button;
        private System.Windows.Forms.TextBox run_every_field;
        private System.Windows.Forms.Label static_text_1;
        private System.Windows.Forms.Label static_text_2;
        private System.Windows.Forms.Button load_button;
        private System.Windows.Forms.Label runtime_label;
        private System.Windows.Forms.ProgressBar progress_bar;
        private System.Windows.Forms.Label status_label;
        private System.Windows.Forms.Label static_label_status;
        private System.Windows.Forms.Label static_label_action_count;
        private System.Windows.Forms.Label action_count_label;
        private System.Windows.Forms.Button clear_events_button;
    }
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Configure_image_processor
{
    [Serializable]
    public class Match
    {
        public int matchID;
        public TimeSpan gameTime;
        public List<Team> teams;
        public DateTime timeStamp;

        public Match() 
        {
            this.matchID     = 0;
            this.gameTime    = new TimeSpan();
            this.teams       = new List<Team>();
            this.timeStamp   = new DateTime();
            
        }
        public Match(Match matchToCopy)
        {
            this.matchID = matchToCopy.matchID;
            this.gameTime = matchToCopy.gameTime;
            this.timeStamp = matchToCopy.timeStamp;
            this.teams = new List<Team>();

            //clone the teams
            foreach (Team team in matchToCopy.teams)
            {
                Team aTeam = new Team(team);
                this.teams.Add(aTeam);
            }    
        
        }

    }

}
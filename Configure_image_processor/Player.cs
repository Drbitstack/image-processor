﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configure_image_processor
{
    [Serializable]
    public class Player
    {
        public List<indexedString> attributes;
        /*
         * Attribute indicies description:
         * 
         * 0: NAME
         * 1: HERO NAME
         * 2: CURRENT LEVEL
         * 3: CURRENT GOLD
         * 4: GOLD/MINUTE
         * 5: XP/MINUTE
         * 
         */
    
        public Player()
        {
            this.attributes = new List<indexedString>();
        }
        public Player(Player clonePlayer) 
        {
            this.attributes = new List<indexedString>();

            foreach(indexedString attr in clonePlayer.attributes)
            {
                indexedString anIString = new indexedString(attr);
                this.attributes.Add(anIString);
            }

        }
    }
}

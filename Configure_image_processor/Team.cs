﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configure_image_processor
{
    [Serializable]
    public class Team
    {
        public List<Player> players;
        public List<Tower>  towers;
        public List<indexedString> attributes; 
        /*
         * Attribute indicies description:
         * 
         * 0: NAME
         */

        public Team() 
        {
            this.players = new List<Player>();
            this.towers = new List<Tower>();
            this.attributes = new List<indexedString>();
        }

        public Team (Team cloneTeam) 
        {
            
            this.players = new List<Player>();
            this.towers = new List<Tower>();
            this.attributes = new List<indexedString>();

            //mirror it
            foreach (Player player in cloneTeam.players)
            {
                Player aPlayer = new Player(player);
                this.players.Add(aPlayer);
            }
            foreach (Tower tower in cloneTeam.towers)
            {
                Tower aTower = new Tower(tower);
                this.towers.Add(tower);
            }
            foreach (indexedString attribute in cloneTeam.attributes) 
            {
                indexedString anIString = new indexedString(attribute); 
                this.attributes.Add(attribute);
            }

        }

    }
}

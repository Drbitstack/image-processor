﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Configure_image_processor
{
    [Serializable]
    public class selectedArea
    {
        //needs 4 points, 2 for the original selection, 2 for the translated selection. 
        //Also needs a color & threshold value
        public System.Drawing.Point startPoint{ get; set; }
        public System.Drawing.Point endPoint { get; set; }
        public System.Drawing.Point translatedStartPoint { get; set; }
        public System.Drawing.Point translatedEndPoint { get; set; }
        public System.Drawing.Color chosenColor { get; set; }
        public System.Drawing.Color readColor { get; set; }
        public bool isOCR { get; set; }
        public int colorThreshold { get; set; }
        public int myIndex { get; set; }
        
        public selectedArea()
        {
            this.startPoint      = new System.Drawing.Point();
            this.endPoint        = new System.Drawing.Point();
            this.chosenColor     = new System.Drawing.Color();
            this.readColor       = new System.Drawing.Color();
            this.isOCR           = false;
            this.myIndex         = -1;
            this.colorThreshold = 30;
        }

        public selectedArea(selectedArea areaToCopy) 
        {
            this.startPoint     = areaToCopy.startPoint; 
            this.endPoint       = areaToCopy.endPoint;
            this.chosenColor    = areaToCopy.chosenColor;
            this.readColor      = areaToCopy.readColor;
            this.isOCR          = areaToCopy.isOCR;
            this.myIndex        = areaToCopy.myIndex;
            this.colorThreshold = areaToCopy.colorThreshold;
        }

        public selectedArea(Point startingPoint, Point endingPoint, int index, Color color, bool ocr, int threshold)
        {
            this.startPoint      = startingPoint;
            this.endPoint        = endingPoint;
            this.chosenColor     = color;
            this.readColor       = color;
            this.myIndex         = index;
            this.isOCR           = ocr;
            this.colorThreshold = threshold;
        }

    }
}

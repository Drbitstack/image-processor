﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configure_image_processor
{
    public static class global_constants
    {
        public static int TEAMS_IN_MATCH = 2;
        public static int PLAYERS_ON_TEAM = 5;
        public static int DATA_IN_PLAYER = 5;
        public static int TOWERS_PER_TEAM = 11;
    }
}
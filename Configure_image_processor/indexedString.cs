﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configure_image_processor
{
    [Serializable]
    public class indexedString
    {
        public string text    { get; set; }
        public int index      { get; set; }

        public indexedString() 
        {
            this.text  = "";
            this.index = -1;
        }
        public indexedString(indexedString cloneString)
        {
            this.text = cloneString.text;
            this.index = cloneString.index;
        }
        public indexedString(string text, int index) 
        {
            this.text = text;
            this.index = index;
        }

    }
}

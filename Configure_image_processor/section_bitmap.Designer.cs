﻿namespace Configure_image_processor
{
    partial class section_bitmap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.done_button = new System.Windows.Forms.Button();
            this.select_tool_radio = new System.Windows.Forms.RadioButton();
            this.eyedrop_tool_radio = new System.Windows.Forms.RadioButton();
            this.tool_group = new System.Windows.Forms.GroupBox();
            this.ocr_checkbox = new System.Windows.Forms.CheckBox();
            this.static_cursorxy = new System.Windows.Forms.Label();
            this.cursor_position_field = new System.Windows.Forms.TextBox();
            this.select_info_group = new System.Windows.Forms.GroupBox();
            this.static_color_label = new System.Windows.Forms.Label();
            this.size_label = new System.Windows.Forms.Label();
            this.xy_label = new System.Windows.Forms.Label();
            this.color_select_text = new System.Windows.Forms.TextBox();
            this.threshold_text = new System.Windows.Forms.TextBox();
            this.wh_select_text = new System.Windows.Forms.TextBox();
            this.xy_select_text = new System.Windows.Forms.TextBox();
            this.selection_count_label = new System.Windows.Forms.Label();
            this.undo_button = new System.Windows.Forms.Button();
            this.repaint_button = new System.Windows.Forms.Button();
            this.clear_selections_button = new System.Windows.Forms.Button();
            this.ratio_lock_checkbox = new System.Windows.Forms.CheckBox();
            this.collect_color_button = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.data_area_header_label = new System.Windows.Forms.Label();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.data_listview = new System.Windows.Forms.ListView();
            this.item_column = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.index_column = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.import_match_button = new System.Windows.Forms.Button();
            this.generate_item_list_button = new System.Windows.Forms.Button();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.control_panel_groupBox = new System.Windows.Forms.GroupBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearSelectedItemBindingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tool_group.SuspendLayout();
            this.select_info_group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.control_panel_groupBox.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // done_button
            // 
            this.done_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.done_button.Location = new System.Drawing.Point(840, 90);
            this.done_button.Name = "done_button";
            this.done_button.Size = new System.Drawing.Size(114, 23);
            this.done_button.TabIndex = 5;
            this.done_button.Text = "Done";
            this.done_button.UseVisualStyleBackColor = true;
            this.done_button.Click += new System.EventHandler(this.done_button_Click);
            // 
            // select_tool_radio
            // 
            this.select_tool_radio.AutoSize = true;
            this.select_tool_radio.Location = new System.Drawing.Point(15, 49);
            this.select_tool_radio.Name = "select_tool_radio";
            this.select_tool_radio.Size = new System.Drawing.Size(64, 17);
            this.select_tool_radio.TabIndex = 6;
            this.select_tool_radio.TabStop = true;
            this.select_tool_radio.Text = "Selector";
            this.select_tool_radio.UseVisualStyleBackColor = true;
            this.select_tool_radio.CheckedChanged += new System.EventHandler(this.select_tool_radio_CheckedChanged);
            // 
            // eyedrop_tool_radio
            // 
            this.eyedrop_tool_radio.AutoSize = true;
            this.eyedrop_tool_radio.Location = new System.Drawing.Point(15, 24);
            this.eyedrop_tool_radio.Name = "eyedrop_tool_radio";
            this.eyedrop_tool_radio.Size = new System.Drawing.Size(79, 17);
            this.eyedrop_tool_radio.TabIndex = 7;
            this.eyedrop_tool_radio.TabStop = true;
            this.eyedrop_tool_radio.Text = "Eyedropper";
            this.eyedrop_tool_radio.UseVisualStyleBackColor = true;
            this.eyedrop_tool_radio.CheckedChanged += new System.EventHandler(this.eyedrop_tool_radio_CheckedChanged);
            // 
            // tool_group
            // 
            this.tool_group.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tool_group.Controls.Add(this.ocr_checkbox);
            this.tool_group.Controls.Add(this.static_cursorxy);
            this.tool_group.Controls.Add(this.cursor_position_field);
            this.tool_group.Controls.Add(this.eyedrop_tool_radio);
            this.tool_group.Controls.Add(this.select_tool_radio);
            this.tool_group.Location = new System.Drawing.Point(17, 14);
            this.tool_group.Name = "tool_group";
            this.tool_group.Size = new System.Drawing.Size(200, 99);
            this.tool_group.TabIndex = 10;
            this.tool_group.TabStop = false;
            this.tool_group.Text = "Tools";
            // 
            // ocr_checkbox
            // 
            this.ocr_checkbox.AutoSize = true;
            this.ocr_checkbox.Location = new System.Drawing.Point(15, 76);
            this.ocr_checkbox.Name = "ocr_checkbox";
            this.ocr_checkbox.Size = new System.Drawing.Size(102, 17);
            this.ocr_checkbox.TabIndex = 10;
            this.ocr_checkbox.Text = "OCR Selection?";
            this.ocr_checkbox.UseVisualStyleBackColor = true;
            this.ocr_checkbox.CheckedChanged += new System.EventHandler(this.ocr_checkbox_CheckedChanged);
            // 
            // static_cursorxy
            // 
            this.static_cursorxy.AutoSize = true;
            this.static_cursorxy.Location = new System.Drawing.Point(130, 41);
            this.static_cursorxy.Name = "static_cursorxy";
            this.static_cursorxy.Size = new System.Drawing.Size(56, 13);
            this.static_cursorxy.TabIndex = 9;
            this.static_cursorxy.Text = "cursor X,Y";
            // 
            // cursor_position_field
            // 
            this.cursor_position_field.Location = new System.Drawing.Point(126, 19);
            this.cursor_position_field.Name = "cursor_position_field";
            this.cursor_position_field.Size = new System.Drawing.Size(68, 20);
            this.cursor_position_field.TabIndex = 8;
            // 
            // select_info_group
            // 
            this.select_info_group.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.select_info_group.Controls.Add(this.static_color_label);
            this.select_info_group.Controls.Add(this.size_label);
            this.select_info_group.Controls.Add(this.xy_label);
            this.select_info_group.Controls.Add(this.color_select_text);
            this.select_info_group.Controls.Add(this.threshold_text);
            this.select_info_group.Controls.Add(this.wh_select_text);
            this.select_info_group.Controls.Add(this.xy_select_text);
            this.select_info_group.Location = new System.Drawing.Point(223, 14);
            this.select_info_group.Name = "select_info_group";
            this.select_info_group.Size = new System.Drawing.Size(390, 99);
            this.select_info_group.TabIndex = 11;
            this.select_info_group.TabStop = false;
            this.select_info_group.Text = "Selection Info.";
            // 
            // static_color_label
            // 
            this.static_color_label.AutoSize = true;
            this.static_color_label.Location = new System.Drawing.Point(6, 77);
            this.static_color_label.Name = "static_color_label";
            this.static_color_label.Size = new System.Drawing.Size(114, 13);
            this.static_color_label.TabIndex = 5;
            this.static_color_label.Text = "Color [Val] [Threshold] ";
            // 
            // size_label
            // 
            this.size_label.AutoSize = true;
            this.size_label.Location = new System.Drawing.Point(6, 52);
            this.size_label.Name = "size_label";
            this.size_label.Size = new System.Drawing.Size(72, 13);
            this.size_label.TabIndex = 4;
            this.size_label.Text = "Width, Height";
            // 
            // xy_label
            // 
            this.xy_label.AutoSize = true;
            this.xy_label.Location = new System.Drawing.Point(6, 22);
            this.xy_label.Name = "xy_label";
            this.xy_label.Size = new System.Drawing.Size(66, 13);
            this.xy_label.TabIndex = 3;
            this.xy_label.Text = "X, Y (center)";
            // 
            // color_select_text
            // 
            this.color_select_text.Location = new System.Drawing.Point(122, 72);
            this.color_select_text.Name = "color_select_text";
            this.color_select_text.Size = new System.Drawing.Size(208, 20);
            this.color_select_text.TabIndex = 2;
            this.color_select_text.TextChanged += new System.EventHandler(this.color_select_text_TextChanged);
            // 
            // threshold_text
            // 
            this.threshold_text.Location = new System.Drawing.Point(330, 72);
            this.threshold_text.Name = "threshold_text";
            this.threshold_text.Size = new System.Drawing.Size(54, 20);
            this.threshold_text.TabIndex = 12;
            this.threshold_text.TextChanged += new System.EventHandler(this.threshold_text_TextChanged);
            // 
            // wh_select_text
            // 
            this.wh_select_text.Location = new System.Drawing.Point(122, 46);
            this.wh_select_text.Name = "wh_select_text";
            this.wh_select_text.Size = new System.Drawing.Size(100, 20);
            this.wh_select_text.TabIndex = 1;
            this.wh_select_text.TextChanged += new System.EventHandler(this.wh_select_text_TextChanged);
            // 
            // xy_select_text
            // 
            this.xy_select_text.Location = new System.Drawing.Point(122, 19);
            this.xy_select_text.Name = "xy_select_text";
            this.xy_select_text.Size = new System.Drawing.Size(100, 20);
            this.xy_select_text.TabIndex = 0;
            this.xy_select_text.TextChanged += new System.EventHandler(this.xy_select_text_TextChanged);
            this.xy_select_text.Leave += new System.EventHandler(this.xy_selectText_leave);
            // 
            // selection_count_label
            // 
            this.selection_count_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.selection_count_label.AutoSize = true;
            this.selection_count_label.Location = new System.Drawing.Point(719, 16);
            this.selection_count_label.Name = "selection_count_label";
            this.selection_count_label.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.selection_count_label.Size = new System.Drawing.Size(66, 13);
            this.selection_count_label.TabIndex = 13;
            this.selection_count_label.Text = " 0 selections";
            // 
            // undo_button
            // 
            this.undo_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.undo_button.Location = new System.Drawing.Point(722, 62);
            this.undo_button.Name = "undo_button";
            this.undo_button.Size = new System.Drawing.Size(112, 23);
            this.undo_button.TabIndex = 14;
            this.undo_button.Text = "Undo Last Selction";
            this.undo_button.UseVisualStyleBackColor = true;
            this.undo_button.Click += new System.EventHandler(this.undo_button_Click);
            // 
            // repaint_button
            // 
            this.repaint_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.repaint_button.Location = new System.Drawing.Point(619, 91);
            this.repaint_button.Name = "repaint_button";
            this.repaint_button.Size = new System.Drawing.Size(97, 23);
            this.repaint_button.TabIndex = 15;
            this.repaint_button.Text = "repaint (debug)";
            this.repaint_button.UseVisualStyleBackColor = true;
            this.repaint_button.Click += new System.EventHandler(this.repaint_button_Click);
            // 
            // clear_selections_button
            // 
            this.clear_selections_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.clear_selections_button.Location = new System.Drawing.Point(722, 90);
            this.clear_selections_button.Name = "clear_selections_button";
            this.clear_selections_button.Size = new System.Drawing.Size(112, 23);
            this.clear_selections_button.TabIndex = 16;
            this.clear_selections_button.Text = "Clear Selections";
            this.clear_selections_button.UseVisualStyleBackColor = true;
            this.clear_selections_button.Click += new System.EventHandler(this.clear_selections_button_Click);
            // 
            // ratio_lock_checkbox
            // 
            this.ratio_lock_checkbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ratio_lock_checkbox.AutoSize = true;
            this.ratio_lock_checkbox.Enabled = false;
            this.ratio_lock_checkbox.Location = new System.Drawing.Point(870, 14);
            this.ratio_lock_checkbox.Name = "ratio_lock_checkbox";
            this.ratio_lock_checkbox.Size = new System.Drawing.Size(69, 17);
            this.ratio_lock_checkbox.TabIndex = 17;
            this.ratio_lock_checkbox.Text = "lock ratio";
            this.ratio_lock_checkbox.UseVisualStyleBackColor = true;
            this.ratio_lock_checkbox.CheckedChanged += new System.EventHandler(this.lock_ratio_checkbox_Event);
            // 
            // collect_color_button
            // 
            this.collect_color_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.collect_color_button.Location = new System.Drawing.Point(619, 62);
            this.collect_color_button.Name = "collect_color_button";
            this.collect_color_button.Size = new System.Drawing.Size(97, 23);
            this.collect_color_button.TabIndex = 18;
            this.collect_color_button.Text = "collect center color";
            this.collect_color_button.UseVisualStyleBackColor = true;
            this.collect_color_button.Click += new System.EventHandler(this.collect_color_button_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.AllowDrop = true;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1129, 806);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.SizeChanged += new System.EventHandler(this.resize_picturebox_Event);
            this.pictureBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.picturebox_dragDrop);
            this.pictureBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.picturebox_dragEnter);
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.picturebox_paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picturebox_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer1.Panel2MinSize = 959;
            this.splitContainer1.Size = new System.Drawing.Size(1319, 934);
            this.splitContainer1.SplitterDistance = 186;
            this.splitContainer1.TabIndex = 19;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.data_area_header_label);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(186, 934);
            this.splitContainer2.SplitterDistance = 48;
            this.splitContainer2.TabIndex = 0;
            // 
            // data_area_header_label
            // 
            this.data_area_header_label.AutoSize = true;
            this.data_area_header_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.data_area_header_label.Location = new System.Drawing.Point(44, 15);
            this.data_area_header_label.Name = "data_area_header_label";
            this.data_area_header_label.Size = new System.Drawing.Size(54, 24);
            this.data_area_header_label.TabIndex = 0;
            this.data_area_header_label.Text = "Items";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.data_listview);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.import_match_button);
            this.splitContainer3.Panel2.Controls.Add(this.generate_item_list_button);
            this.splitContainer3.Size = new System.Drawing.Size(186, 882);
            this.splitContainer3.SplitterDistance = 755;
            this.splitContainer3.TabIndex = 1;
            // 
            // data_listview
            // 
            this.data_listview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.item_column,
            this.index_column});
            this.data_listview.ContextMenuStrip = this.contextMenuStrip1;
            this.data_listview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.data_listview.Location = new System.Drawing.Point(0, 0);
            this.data_listview.Name = "data_listview";
            this.data_listview.Size = new System.Drawing.Size(186, 755);
            this.data_listview.TabIndex = 0;
            this.data_listview.UseCompatibleStateImageBehavior = false;
            this.data_listview.View = System.Windows.Forms.View.Details;
            this.data_listview.ItemActivate += new System.EventHandler(this.item_activate_Event);
            this.data_listview.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.item_drag_Event);
            this.data_listview.ItemMouseHover += new System.Windows.Forms.ListViewItemMouseHoverEventHandler(this.item_hover_event);
            this.data_listview.DoubleClick += new System.EventHandler(this.item_double_click_Event);
            // 
            // item_column
            // 
            this.item_column.Text = "Item";
            this.item_column.Width = 125;
            // 
            // index_column
            // 
            this.index_column.Text = "Index #";
            this.index_column.Width = 58;
            // 
            // import_match_button
            // 
            this.import_match_button.Location = new System.Drawing.Point(36, 67);
            this.import_match_button.Name = "import_match_button";
            this.import_match_button.Size = new System.Drawing.Size(75, 23);
            this.import_match_button.TabIndex = 1;
            this.import_match_button.Text = "Import";
            this.import_match_button.UseVisualStyleBackColor = true;
            this.import_match_button.Click += new System.EventHandler(this.import_match_button_Click);
            // 
            // generate_item_list_button
            // 
            this.generate_item_list_button.Location = new System.Drawing.Point(36, 31);
            this.generate_item_list_button.Name = "generate_item_list_button";
            this.generate_item_list_button.Size = new System.Drawing.Size(75, 23);
            this.generate_item_list_button.TabIndex = 0;
            this.generate_item_list_button.Text = "Generate Items";
            this.generate_item_list_button.UseVisualStyleBackColor = true;
            this.generate_item_list_button.Click += new System.EventHandler(this.generate_item_list_button_Click);
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.IsSplitterFixed = true;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.pictureBox1);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.control_panel_groupBox);
            this.splitContainer4.Size = new System.Drawing.Size(1129, 934);
            this.splitContainer4.SplitterDistance = 806;
            this.splitContainer4.TabIndex = 20;
            // 
            // control_panel_groupBox
            // 
            this.control_panel_groupBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.control_panel_groupBox.Controls.Add(this.select_info_group);
            this.control_panel_groupBox.Controls.Add(this.tool_group);
            this.control_panel_groupBox.Controls.Add(this.collect_color_button);
            this.control_panel_groupBox.Controls.Add(this.selection_count_label);
            this.control_panel_groupBox.Controls.Add(this.ratio_lock_checkbox);
            this.control_panel_groupBox.Controls.Add(this.done_button);
            this.control_panel_groupBox.Controls.Add(this.repaint_button);
            this.control_panel_groupBox.Controls.Add(this.undo_button);
            this.control_panel_groupBox.Controls.Add(this.clear_selections_button);
            this.control_panel_groupBox.Location = new System.Drawing.Point(67, 3);
            this.control_panel_groupBox.Name = "control_panel_groupBox";
            this.control_panel_groupBox.Size = new System.Drawing.Size(959, 119);
            this.control_panel_groupBox.TabIndex = 19;
            this.control_panel_groupBox.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearSelectedItemBindingsToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(211, 26);
            // 
            // clearSelectedItemBindingsToolStripMenuItem
            // 
            this.clearSelectedItemBindingsToolStripMenuItem.Name = "clearSelectedItemBindingsToolStripMenuItem";
            this.clearSelectedItemBindingsToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.clearSelectedItemBindingsToolStripMenuItem.Text = "Clear Selected Item Bindings";
            this.clearSelectedItemBindingsToolStripMenuItem.Click += new System.EventHandler(this.clearSelectedItemBindingsToolStripMenuItem_Click);
            // 
            // section_bitmap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1319, 934);
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(1325, 825);
            this.Name = "section_bitmap";
            this.Text = "Identify Sections";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.form_closed_Event);
            this.Load += new System.EventHandler(this.section_bitmap_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.keypress_Event);
            this.Resize += new System.EventHandler(this.resize_Event);
            this.tool_group.ResumeLayout(false);
            this.tool_group.PerformLayout();
            this.select_info_group.ResumeLayout(false);
            this.select_info_group.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.control_panel_groupBox.ResumeLayout(false);
            this.control_panel_groupBox.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.Button done_button;
        private System.Windows.Forms.RadioButton select_tool_radio;
        private System.Windows.Forms.RadioButton eyedrop_tool_radio;
        private System.Windows.Forms.GroupBox tool_group;
        private System.Windows.Forms.GroupBox select_info_group;
        private System.Windows.Forms.Label static_color_label;
        private System.Windows.Forms.Label size_label;
        private System.Windows.Forms.Label xy_label;
        private System.Windows.Forms.TextBox color_select_text;
        private System.Windows.Forms.TextBox wh_select_text;
        private System.Windows.Forms.TextBox xy_select_text;
        private System.Windows.Forms.TextBox threshold_text;
        private System.Windows.Forms.Label selection_count_label;
        private System.Windows.Forms.Button undo_button;
        private System.Windows.Forms.Button repaint_button;
        private System.Windows.Forms.Button clear_selections_button;
        private System.Windows.Forms.Label static_cursorxy;
        private System.Windows.Forms.TextBox cursor_position_field;
        private System.Windows.Forms.CheckBox ocr_checkbox;
        private System.Windows.Forms.CheckBox ratio_lock_checkbox;
        private System.Windows.Forms.Button collect_color_button;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox control_panel_groupBox;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label data_area_header_label;
        private System.Windows.Forms.ListView data_listview;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Button generate_item_list_button;
        private System.Windows.Forms.Button import_match_button;
        private System.Windows.Forms.ColumnHeader item_column;
        private System.Windows.Forms.ColumnHeader index_column;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem clearSelectedItemBindingsToolStripMenuItem;
    }
}
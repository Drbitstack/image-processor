﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configure_image_processor
{
    [Serializable]
    public class indexedInt
    {
       public int val   { get; set; }
       public int index { get; set; }
       public indexedInt()
       {
           this.val   = -1;
           this.index = -1;
       }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configure_image_processor
{
    [Serializable]
    public class save_context
    {
        public Match finalMatchInfo;
        public List<selectedArea> finalSelections;
        public System.Drawing.Size finalSizeOfConfigureScreen;

        public save_context()
        {
            this.finalMatchInfo = new Match();
            this.finalSelections = new List<selectedArea>();
            this.finalSizeOfConfigureScreen = new System.Drawing.Size();
        }
        public save_context(Match match, List<selectedArea> selections, System.Drawing.Size size) 
        {
            this.finalMatchInfo = new Match(match);//copy the match to me
            this.finalSelections = new List<selectedArea>(selections);//does this work?
            this.finalSizeOfConfigureScreen = size;
        }

    }
}

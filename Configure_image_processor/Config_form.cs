﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml.Serialization;

namespace Configure_image_processor
{
    public partial class Config_form : Form
    {
        private string imageFile;

        /*-----------Timing Items-------------------*/
        private System.Windows.Forms.Timer run_timer = new System.Windows.Forms.Timer();   //responsible for performing periodic actions
        private bool timerRunState { get; set; }                                           //global state of run timer
        DateTime timerStartTime;                                                           //the starting time of the timer...not currently used
        Stopwatch stopWatch = new Stopwatch();                                             //used for tracking elapsed time since timer was started
        private int performAction_TimeInterval, actionTickCount, ticksPerPeriod, eventTickCount;
        private double millsecondsOfLastAction;
        /***********************************************/

        //user save storage
        private List<selectedArea> finalSelections = new List<selectedArea>();
        private Match matchImport = new Match();
        //OCR data storage
        List<List<tessnet2.Word>> wordListList = new List<List<tessnet2.Word>>();
        List<string> stringList = new List<string>();

        //first processing loop
        private bool firstLoop = true;
        private int save_bandaid_index = 1;

        private Size exitingSize = Size.Empty;
        /*------------------Pixel Color Collection----------------------
        [DllImport("Gdi32.dll")]
        public static extern int GetPixel(
        System.IntPtr hdc,    // handle to DC
        int nXPos,  // x-coordinate of pixel
        int nYPos   // y-coordinate of pixel
        );

        [DllImport("User32.dll")]
        public static extern IntPtr GetDC(IntPtr wnd);

        [DllImport("User32.dll")]
        public static extern void ReleaseDC(IntPtr dc);
        ****************************************************************/
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAsAttribute(UnmanagedType.Bool)]


        static extern bool AllocConsole();

        public Config_form()
        {
            InitializeComponent();
            identify_section_button.Enabled = false;
        }

        private void Config_form_Load(object sender, EventArgs e)
        {
            this.status_label.ForeColor = Color.Red;
            save_button.Enabled = false;
            stop_button.Enabled = false;
            eventTickCount = 0;
            ticksPerPeriod = 5; //hard coded progess thingy
            run_timer.Tick += new EventHandler(run_timer_tick_event);
            AllocConsole();
            //import();
        }

        #region Selection Window Form
        #endregion

        #region Events

        #region Button Events

        private void identify_section_button_Click(object sender, EventArgs e)
        {

            using (section_bitmap image_form = new section_bitmap(imageFile, finalSelections, exitingSize, matchImport))
            {
                image_form.ShowDialog();
                finalSelections = image_form.selectionList;
                matchImport = image_form.matchData;
                exitingSize = image_form.sizeOnExit;
            }

            if (finalSelections.Count > 0) this.save_button.Enabled = true;

        }

        private void select_image_button_Click(object sender, EventArgs e)
        {
            using (var fDialog = new System.Windows.Forms.OpenFileDialog())
            {
                fDialog.Filter = "Bitmap Files (*.bmp)|*.bmp";
                fDialog.Title = "Select the image for analysis";

                if (fDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //Nothing for now...
                }

                imageFile = fDialog.FileName;
                working_image_text.Text = imageFile;
            }
        }

        private void clear_events_button_Click(object sender, EventArgs e)
        {
            resetTickLabel();
        }
        private void go_button_Click(object sender, EventArgs e)
        {

            string runTimeText = this.run_every_field.Text;
            int runtimeValue = 1000; //default 1s

            if (runTimeText.Length > 0)
            {
                try { runtimeValue = int.Parse(runTimeText); }
                catch { MessageBox.Show("Please enter a valid number > 100ms"); }
            }

            performAction_TimeInterval = (runtimeValue < 100) ? 1000 : runtimeValue;
            this.run_every_field.Text = performAction_TimeInterval + "";

            startTimer(performAction_TimeInterval / ticksPerPeriod);//start action timer
            //start progress timer

        }

        private void stop_button_Click(object sender, EventArgs e)
        {
            stopTimer();
            //firstLoop = true;
        }

        private void load_button_Click(object sender, EventArgs e)
        {
            //deserialize the saved objects in the text file.
            using (var fDialog = new System.Windows.Forms.OpenFileDialog())
            {
                fDialog.Filter = "Image Config Files (*.imcfg)|*.imcfg";
                fDialog.Title = "Select the image for analysis";

                if (fDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) { }

                //if you can even call it sanitation...
                if (fDialog.FileName.Length > 0)
                {
                    loadConfig(fDialog.FileName);
                } 
            }

        }
        private void save_button_Click(object sender, EventArgs e)
        {
            //serialize the saved objects.
            //get file basename
            FileInfo fi = new FileInfo(imageFile);
            string timeStamp = generateTimeStamp();
            string picBaseDir = fi.Directory + "";
            string saveAs;

            using (var fDialog = new System.Windows.Forms.SaveFileDialog())
            {
                fDialog.Title = "Save Configuration As";
                fDialog.InitialDirectory = picBaseDir;
                fDialog.DefaultExt = ".imcfg";
                fDialog.Filter = "Config Files|*.imcfg";

                if (fDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //Nothing for now...
                }

                saveAs = fDialog.FileName;
            }

            save_context context = new save_context(matchImport, finalSelections, exitingSize);

            //create serializer stuff
            IFormatter formatter = new BinaryFormatter();
            //XmlSerializer formatter = new XmlSerializer(typeof(save_context));
            //picBaseDir + "\\saved_config_" + fileOutputName + ".imcfg"
            if (saveAs.Length > 0)
            {
                Stream stream = new FileStream(saveAs, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, context);
                stream.Close();
            }

        }

        #endregion

        #region Timer Tick Events
        private void run_timer_tick_event(object sender, EventArgs e)
        {
            TimeSpan ts = stopWatch.Elapsed;
            eventTickCount++;
            doProgress(ts);
            updateRuntimeLabel(ts);
            if (isTimeForAction(ts) && finalSelections.Count > 0 && matchImport != null) //time for an event eventTickCount % ticksPerPeriod == 0 ?  - won't work if actions take a long time
            {
                takeAction(ts);
            }

        }

        private bool isTimeForAction(TimeSpan ts)
        {
            return ((ts.TotalMilliseconds - millsecondsOfLastAction) >= performAction_TimeInterval);
        }

        #endregion

        #region Radio Events

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {


        }


        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {


        }

        #endregion

        #region Text Field Events


        private void working_image_text_TextChanged(object sender, EventArgs e)
        {

            if (working_image_text.Text.Length > 0)
            {
                identify_section_button.Enabled = true;
            }
            else
            {
                identify_section_button.Enabled = false;
            }

            //string server = Properties.Settings.Default.Server;
            //sections_text.Text = server;

        }

        #endregion

        #endregion

        #region Action Related Functions


        private void uploadData()
        {

            /*
             * public static int insertPlayerSnapshot(SqlConnection sqlConn, int PlayerID, 
                                            int matchID, int heroID, int level, int currentGold, 
                                            int gpm, int xpm, TimeSpan gameTime)
             * 
             * public static int insertMatchSnapshot(SqlConnection sqlConn, int matchId, String rTowerStatus, String rBarrackStatus, bool rAncientStatus, 
                                       String dTowerStatus, String dBarrackStatus, bool dAncientStatus, TimeSpan gameTime)
             * 
             * 
             * public static DataTable getPlayerKeys(SqlConnection sqlConn, int matchId)
             * 
             * 
             */
            
            SqlConnection con = new SqlConnection();
            /*
            con.ConnectionString = Properties.Settings.Default.Connection;

            DataTable playerKeyTable = DotaWrapperLibrary.SqlWrapper.getPlayerKeys(con, matchImport.matchID);
             * 
            bool success = DotaWrapperLibrary.SqlWrapper.insertMatchSnapshot( con, matchImport.matchID, rTowerStatus, String rBarrackStatus, bool rAncientStatus, 
                                       String dTowerStatus, String dBarrackStatus, bool dAncientStatus, TimeSpan gameTime);


            */
            con.ConnectionString = "";



            /*
            SqlCommand commander = new SqlCommand();
            commander.Connection = con;

            commander.CommandType = CommandType.StoredProcedure;
            commander.CommandText = "sp_insetMatchSnapShot";
            commander.Parameters.Add("@matchID", SqlDbType.Int).Value = matchID"";

            commander.ExecuteNonQuery();

            commander.Parameters.Clear();
            commander.CommandText = "sp_insetMatchSnapShot";
            commander.Parameters.Add("@matchID", SqlDbType.Int).Value = matchID"";
            commander.ExecuteNonQuery();*/
            
            /*...*/
            con.Close();


        }

        private void processImage(string imageName)
        {
            
            int OCRCount = 0, colorCount = 0, selectionIndex;
                        
            //lookup table 
            int[,] selectionLUT = new int[finalSelections.Count, 3];
            /*
             * selectionLUT [i] = {true index, OCR index, color index}
             */

            using (Bitmap image_bmp = new Bitmap(imageName)) 
            {

                //for each selection that is OCR, add a word list to a list of word lists.
                
                for (int i = 0; i < finalSelections.Count; i++)
                {
                    if (finalSelections[i].isOCR)
                    {
                        wordListList.Add(selectionTextRecognition(i, image_bmp));
                        selectionLUT[i, 0] = i;
                        selectionLUT[i, 1] = OCRCount++;
                        selectionLUT[i, 2] = -1;
                    }
                    else
                    {
                        stringList.Add(selectionColorMonitorStatus(i, image_bmp));
                        selectionLUT[i, 0] = i;
                        selectionLUT[i, 1] = -1;
                        selectionLUT[i, 2] = colorCount++;
                    }
                }
                firstLoop = false;
                
                #region populate the match data from selection data

                for (int i = 0; i < global_constants.TEAMS_IN_MATCH; i++)
                {
                    selectionIndex = matchImport.teams[i].attributes[0].index;
                    if (selectionIndex >= 0)
                    {
                        if (finalSelections[selectionIndex].isOCR)
                        {
                            matchImport.teams[i].attributes[0].text = myWordList(selectionIndex, wordListList, selectionLUT, false);
                            Console.WriteLine("I am Team " + (i + 1) + "'s Name: " + matchImport.teams[i].attributes[0].text);
                        }
                        else
                        {
                            Console.WriteLine("DEBUG: selection # " + (selectionIndex + 1) + " should be checked OCR, but is currently not.");
                        }
                    }
                    for (int j = 0; j < global_constants.PLAYERS_ON_TEAM; j++)//all players
                    {
                        for (int k = 0; k < global_constants.DATA_IN_PLAYER; k++)//all stats of the player
                        {
                            selectionIndex = matchImport.teams[i].players[j].attributes[k].index;
                            if (selectionIndex >= 0)
                            {
                                if (finalSelections[selectionIndex].isOCR)
                                {
                                    matchImport.teams[i].players[j].attributes[k].text = myWordList(selectionIndex, wordListList, selectionLUT, true);
                                }
                                else
                                {
                                    Console.WriteLine("DEBUG: selection # " + (selectionIndex + 1) + " should be checked OCR, but is currently not.");
                                }
                                Console.WriteLine("I am Team " + (i + 1) + "'s Player " + (j + 1) + "'s Data # " + (k + 1) + ": " + matchImport.teams[i].players[j].attributes[k].text);
                            }

                        }
                    }
                    for (int l = 0; l < (global_constants.TOWERS_PER_TEAM); l++)//the towers-2 for acient & barracks
                    {
                        selectionIndex = matchImport.teams[i].towers[l].status.index;
                        if (selectionIndex >= 0)
                        {
                            matchImport.teams[i].towers[l].status.text = myStatusString(selectionIndex, stringList, selectionLUT);
                            Console.WriteLine("I am Team " + (i + 1) + "'s Tower # " + (l + 1) + ": " + matchImport.teams[i].towers[l].status.text);
                        }
                    }
                }

                #endregion

            }
            //processing done, clear the lists
            wordListList.Clear();
            stringList.Clear();

        }

        private string myStatusString(int selectionIndex, List<string> stringList, int[,] selectionLUT)
        {
            int dataIndex = selectionLUT[selectionIndex, 2];           
            return stringList[dataIndex];
        }

        private string myWordList(int selectionIndex, List<List<tessnet2.Word>> wordListList, int[,] selectionLUT, bool digitOnly)
        {
            int dataIndex = selectionLUT[selectionIndex, 1];
            string outputString = string.Empty;
            string cleanedWord = string.Empty;

            foreach (tessnet2.Word words in wordListList[dataIndex])
            {
                if (digitOnly) cleanedWord = new string(words.Text.Where(c => char.IsDigit(c)).ToArray()); //clean it
                else cleanedWord = words.Text; //don't clean it
                outputString += cleanedWord;
            }
            return outputString;
        }

        private string selectionColorMonitorStatus(int selectionIndex, Bitmap image_bmp)
        {
            string selectionMonitorData;
            Point centerPoint = getCenterOfRectangle(finalSelections[selectionIndex].translatedStartPoint, finalSelections[selectionIndex].translatedEndPoint);

            Color gottenColor = image_bmp.GetPixel(centerPoint.X, centerPoint.Y);
            if (firstLoop) finalSelections[selectionIndex].readColor = gottenColor; //store gotten color as a base point ; disregard actual inputs
            #region if using color average for recognition, uncomment this stuff
            /*Crop & average method...
      
      Bitmap croppedSelectionBitmap = cropImage(image_bmp,
                                                              makeRectangleFromPoints(
                                                              new Point((centerPoint.X - 2), (centerPoint.Y - 2)), 
                                                              new Point((centerPoint.X+4), (centerPoint.Y+4))));
      //debugging croppedSelectionBitmap.Save("test_save_"+i+".jpg", ImageFormat.Jpeg);
      int selRedAvg = 0;
      int selGreenAvg = 0;
      int selBlueAvg = 0;
      unsafe
      {
          getCroppedRGBAverages(croppedSelectionBitmap, &selRedAvg, &selGreenAvg, &selBlueAvg);
      }
      
      croppedSelectionBitmap.Dispose();*/
            #endregion
            //test the colors & set flag
            selectionMonitorData = doColorsMatch(gottenColor, finalSelections[selectionIndex].readColor,
                                                    finalSelections[selectionIndex].colorThreshold) ? "1" : "0";

            /*----debugging----------*/
            string readOut = String.Format("Read Color:{0},{1},{2} Stored Color: {3},{4},{5}",
                                                   gottenColor.R, gottenColor.G, gottenColor.B, //centralColor.R, centralColor.G, centralColor.B,
            finalSelections[selectionIndex].chosenColor.R, finalSelections[selectionIndex].chosenColor.G, finalSelections[selectionIndex].chosenColor.B);
            //Console.WriteLine(readOut); //ColorTranslator.ToHtml(finalSelections[i].chosenColor)        
            //Console.WriteLine("Color averages are: {0},{1},{2}", selRedAvg, selGreenAvg, selBlueAvg);
            /*-----------------------*/

            //Console.WriteLine(selectionMonitorData);

            return selectionMonitorData;
        }

        private List<tessnet2.Word> selectionTextRecognition(int selectionIndex, Bitmap image_bmp)
        {
            List<tessnet2.Word> result;
            using(tessnet2.Tesseract ocr = new tessnet2.Tesseract())
            {
                ocr.Init("tessdata", "eng", false);
                //make its rectangle crop
                Rectangle selectionRectangle =
                    makeRectangleFromPoints(finalSelections[selectionIndex].translatedStartPoint, finalSelections[selectionIndex].translatedEndPoint);
                //crop it
                using(Bitmap croppedSelection = cropImage(image_bmp, selectionRectangle))
                {                                   //resize it
                    using(Bitmap enlargedSelection = smartResize(croppedSelection))
                    {
                        
                        //debugging
                        //croppedSelection.Save("cropped_"+selectionIndex+".bmp");
                        //enlargedSelection.Save("enlarged_" + selectionIndex + ".bmp");

                        //give it to the OCR thing
                        result = ocr.DoOCR(enlargedSelection, Rectangle.Empty);
                        // foreach (tessnet2.Word word in result)
                        //    Console.Write("{0} "/*: {1}", word.Confidence*/, word.Text);
                    }
                }             
            }
            return result;    
        }

        private Bitmap smartResize(Bitmap toBeResized)
        {
            int height = toBeResized.Height;
            int width = toBeResized.Width;

            int resize_seed = 10;

            //don't let it get bigger than like 7MP? arbitrarily.
            while (height * width * resize_seed * resize_seed > 7000000)
            {
                --resize_seed;
            }

            //TODO
            return new Bitmap(toBeResized, new Size(width * resize_seed, height * resize_seed));

        }
#if false
               // [DllImport("tessnet2_64.dll")]
       // static extern void import();
        private void processText(Bitmap imageToProcess)
        {

            //E:\Users\Matt\Documents\Visual Studio 2013\Projects\Configure_image_processor\Configure_image_processor\Resources
            try
            {
                foreach(selectedArea item in finalSelections) //for all selections
                {
                    if (item.isOCR) //is this one text?
                    {//yes

                        List<tessnet2.Word> result;
                        //make its rectangle crop
                        Rectangle selectionRectangle = makeRectangleFromPoints(item.translatedStartPoint,item.translatedEndPoint);
                        //crop it
                        Bitmap croppedSelection = cropImage(imageToProcess, selectionRectangle);
                        //resize it
                        Bitmap enlargedSelection = smartResize(croppedSelection);

                        //croppedSelection.Save("cropped_"+item.myIndex+".bmp");
                        //enlargedSelection.Save("enlarged_" + item.myIndex + ".bmp");

                        //give it to the OCR thing

                        result = ocr.DoOCR(enlargedSelection, Rectangle.Empty);
                        foreach (tessnet2.Word word in result)
                            Console.WriteLine("{0} : {1}", word.Confidence, word.Text);

                    }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(""+e);
            }
        }
        private string processTowers(Bitmap imageToProcess)
        {
            string towerData = "";
            Point centerPoint;

            
            try
            {
                /*---------------Method using special functions, more modular--------------------------*/
                int totalImageBytes = countImageBytes(imageToProcess);
                byte[] r = new byte[totalImageBytes / 3];
                GCHandle rawRedP = GCHandle.Alloc(r, GCHandleType.Pinned);
                byte[] g = new byte[totalImageBytes / 3];
                GCHandle rawGreenP = GCHandle.Alloc(g, GCHandleType.Pinned);
                byte[] b = new byte[totalImageBytes / 3];
                GCHandle rawBlueP = GCHandle.Alloc(b, GCHandleType.Pinned);

                unsafe
                {
                    byte* redPtr = (byte*)rawRedP.AddrOfPinnedObject().ToPointer();
                    byte* greenPtr = (byte*)rawGreenP.AddrOfPinnedObject().ToPointer();
                    byte* bluePtr = (byte*)rawBlueP.AddrOfPinnedObject().ToPointer();
                    getRGBFromBitmap(redPtr, greenPtr, bluePtr, imageToProcess);
                }
                rawRedP.Free();
                rawGreenP.Free();
                rawBlueP.Free();
                /*--------------------------------------------------------------------------*/
                /*---------------Method using in-line code. less modular, but maybe safer--------
                BitmapData bmpData = imageToProcess.LockBits(new Rectangle(0, 0, imageToProcess.Width, imageToProcess.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                IntPtr ptr = bmpData.Scan0;
                int stride = bmpData.Stride;
                imageToProcess.UnlockBits(bmpData);
                int totalImageBytes = countImageBytes(imageToProcess);//total byte counts (heigh x width)

                byte[] rgbValues = new byte[totalImageBytes]; //array of all bytes in image
                byte[] r = new byte[totalImageBytes / 3];
                byte[] g = new byte[totalImageBytes / 3];
                byte[] b = new byte[totalImageBytes / 3];
                int pixelCount = 0;
                Marshal.Copy(ptr, rgbValues, 0, totalImageBytes);

                for (int row = 0; row < imageToProcess.Height; row++)
                {
                    for (int column = 0; column < imageToProcess.Width; ++column, ++pixelCount)
                    {
                        b[pixelCount] = (byte)(rgbValues[(row * stride) + (column * 3) + 0]);
                        g[pixelCount] = (byte)(rgbValues[(row * stride) + (column * 3) + 1]);
                        r[pixelCount] = (byte)(rgbValues[(row * stride) + (column * 3) + 2]);
                    }
                }
                --------------------------------------------------------------------------*/
                for (int i = 0; i < finalSelections.Count; i++)
                {
                    if (!finalSelections[i].isOCR) 
                    {                   
                        centerPoint = getCenterOfRectangle(finalSelections[i].translatedStartPoint, finalSelections[i].translatedEndPoint);
                        int pixelOfInterest = (centerPoint.Y) * imageToProcess.Width + centerPoint.X;
                        //RGB values to a hex string for easy conversion to a Color obj.
                        string colorString = String.Format("#{0:X2}{1:X2}{2:X2}", r[pixelOfInterest], g[pixelOfInterest], b[pixelOfInterest]);
                        Color centralColor = ColorTranslator.FromHtml(colorString);

                        //OR

                        Color gottenColor = imageToProcess.GetPixel(centerPoint.X, centerPoint.Y);
                        if (firstLoop) finalSelections[i].readColor = gottenColor; //store gotten color as a base point ; disregard actual inputs

                        //Crop & average method...
                        
                        Bitmap croppedSelectionBitmap = cropImage(imageToProcess,
                                                                                makeRectangleFromPoints(
                                                                                new Point((centerPoint.X - 2), (centerPoint.Y - 2)), 
                                                                                new Point((centerPoint.X+4), (centerPoint.Y+4))));
                        //debugging croppedSelectionBitmap.Save("test_save_"+i+".jpg", ImageFormat.Jpeg);
                        int selRedAvg = 0;
                        int selGreenAvg = 0;
                        int selBlueAvg = 0;
                        unsafe
                        {
                            getCroppedRGBAverages(croppedSelectionBitmap, &selRedAvg, &selGreenAvg, &selBlueAvg);
                        }

                        croppedSelectionBitmap.Dispose();
                        

                        //test the colors & set flag
                        towerData += doColorsMatch(gottenColor, finalSelections[i].readColor, 
                                                                finalSelections[i].colorThreshold) ? "1" : "0";                   

                        /*----debugging----------*/
                        string readOut = String.Format("Read Color:{0},{1},{2} Stored Color: {3},{4},{5}",
                                                               gottenColor.R, gottenColor.G, gottenColor.B, //centralColor.R, centralColor.G, centralColor.B,
                        finalSelections[i].chosenColor.R, finalSelections[i].chosenColor.G, finalSelections[i].chosenColor.B);
                        Console.WriteLine(readOut); //ColorTranslator.ToHtml(finalSelections[i].chosenColor)        
                        Console.WriteLine("Color averages are: {0},{1},{2}", selRedAvg, selGreenAvg, selBlueAvg);
                        /*-----------------------*/
                        
                        Console.WriteLine(towerData);
                    }
                    
                 }
                firstLoop = false;
            }
            catch
            {
                this.Close(); //suicide
            }

            return towerData; 
        }
#endif
        private void captureScreen()
        {
            DateTime saveTime = DateTime.Now;
            string seconds = saveTime.Second + "";

            /* for copying my window only
            Rectangle bounds = new Rectangle();
            bounds = this.Bounds;
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
                }
                bitmap.Save(imageFile, ImageFormat.Jpeg);
            }*/
            //for copying my primary screen only
            using (Bitmap bmpScreenCapture = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
                                            Screen.PrimaryScreen.Bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bmpScreenCapture))
                {
                    g.CopyFromScreen(Screen.PrimaryScreen.Bounds.X,
                                     Screen.PrimaryScreen.Bounds.Y,
                                     0, 0,
                                     bmpScreenCapture.Size,
                                     CopyPixelOperation.SourceCopy);

                    bmpScreenCapture.Save(imageFile + "_" + save_bandaid_index + ".bmp", ImageFormat.Bmp);

                }
            }
        }
        /*
         * Function: takeAction( Timespan ts ) 
         * 
         * This function is called each time the timing mechanism determines that it's time
         * to perform some actions on the collected data.
         * 
         * Parameters: Timespan
         * 
         * Returns: void
         * 
         * Globals:
         * 
         * imageFile, save_bandaid_index
         */

        private void takeAction(TimeSpan ts)
        {

            //take screenshot          
            captureScreen();
            //process latest screenshot
            processImage(imageFile + "_" + save_bandaid_index + ".bmp");

            ++save_bandaid_index;
            if (save_bandaid_index > 1) save_bandaid_index = 1;//loop it

            //upload to server
            //uploadData();
            actionTick(ts);
        }

        private void actionTick(TimeSpan ts)
        {
            incrementTickLabel();
            millsecondsOfLastAction = ts.TotalMilliseconds;
        }

        private void doProgress(TimeSpan ts)
        {
            //int modulus  = (eventTickCount % ticksPerPeriod)+1;
            //double dicks = ((double)modulus/(double)ticksPerPeriod)*100;
            //this.progress_bar.Value = (int)dicks;
            double dicks = ((ts.TotalMilliseconds - millsecondsOfLastAction) / performAction_TimeInterval) * 100;
            this.progress_bar.Value = ((int)dicks) > 100 ? 100 : ((int)dicks);
        }

        private void incrementTickLabel()
        {
            this.action_count_label.Text = ++actionTickCount + "";
        }

        private void resetTickLabel()
        {
            actionTickCount = 0;
            this.action_count_label.Text = "0";
        }

        #endregion

        #region Timer Functions
        private void startTimer(int timer_interval)
        {
            run_timer.Interval = timer_interval;
            run_timer.Enabled = true;
            timerStartTime = DateTime.Now;
            stopWatch.Start();
            updateStatusLabels(true);
            timerRunState = true;

            this.stop_button.Enabled = true;
            this.go_button.Enabled = false;
        }
        private void stopTimer()
        {
            timerRunState = false;
            stopWatch.Stop();
            stopWatch.Reset();
            millsecondsOfLastAction = 0;
            eventTickCount = 0;         //reset other shit

            run_timer.Enabled = false;
            updateStatusLabels(false);

            this.go_button.Enabled = true;
            this.stop_button.Enabled = false;
        }
        #endregion

        #region Helper Functions

        private Point getCenterOfRectangle(Point firstPoint, Point secondPoint)
        {
            int width = secondPoint.X - firstPoint.X;
            int height = secondPoint.Y - firstPoint.Y;

            return new Point((firstPoint.X) + (width / 2), (firstPoint.Y) + (height / 2));
        }
        private string generateTimeStamp()
        {
            DateTime saveTime = DateTime.Now;
            String sec = saveTime.Second + "";
            String year = saveTime.Year + "";
            String month = saveTime.Month + "";
            String day = saveTime.Day + "";
            String hour = saveTime.Hour + "";
            String min = saveTime.Minute + "";
            return month + day + year + "_" + hour + min + sec + "";
        }



        /*
         * set the actual pixel rgb from Left to Right, i.e., 
         * 
         *          1   2   3   4   5   6  
         *          7   8   9   10  11  12
         *          ...
         *          n-5 n-4 n-3 n-2 n-1 n
         *       
         */
        private unsafe void getRGBFromBitmap(byte* red, byte* green, byte* blue, Bitmap img)
        {

            BitmapData bmpData = img.LockBits(new Rectangle(0, 0, img.Width, img.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            //create a pointer to the first pixel of the image.
            IntPtr ptr = bmpData.Scan0;

            int bytes = bmpData.Stride * img.Height;//total byte counts (heigh x width)

            byte[] rgbValues = new byte[bytes]; //array of all bytes in image

            //copy all of the bytes to the byte array
            Marshal.Copy(ptr, rgbValues, 0, bytes);

            int stride = bmpData.Stride;

            //iterate through the image, separating RGB pixes into separate arrays
            for (int row = 0; row < img.Height; row++)
            {
                for (int column = 0; column < img.Width; column++)
                {
                    *blue++ = (byte)(rgbValues[(row * stride) + (column * 3) + 0]);
                    *green++ = (byte)(rgbValues[(row * stride) + (column * 3) + 1]);
                    *red++ = (byte)(rgbValues[(row * stride) + (column * 3) + 2]);
                }
            }


            img.UnlockBits(bmpData);

        }

        private int countImageBytes(Bitmap im)
        {
            BitmapData bmpData = im.LockBits(new Rectangle(0, 0, im.Width, im.Height), ImageLockMode.ReadWrite, im.PixelFormat);
            im.UnlockBits(bmpData);
            return bmpData.Stride * im.Height;//total byte counts (heigh x width)
        }

        private unsafe void getCroppedRGBAverages(Bitmap croppedSelectionBitmap, int* selRedAvg, int* selGreenAvg, int* selBlueAvg)
        {

            int totalSelectionPixels = croppedSelectionBitmap.Width * croppedSelectionBitmap.Height;


            for (int selRow = 0; selRow < croppedSelectionBitmap.Height; selRow++)
            {
                for (int selColumn = 0; selColumn < croppedSelectionBitmap.Width; selColumn++)
                {
                    Color pixelColor = croppedSelectionBitmap.GetPixel(selColumn, selRow);
                    *selRedAvg += pixelColor.R;
                    *selGreenAvg += pixelColor.G;
                    *selBlueAvg += pixelColor.B;
                }
            }

            *selRedAvg = *selRedAvg / totalSelectionPixels;
            *selGreenAvg = *selGreenAvg / totalSelectionPixels;
            *selBlueAvg = *selBlueAvg / totalSelectionPixels;
        }

        private Rectangle makeRectangleFromPoints(Point point1, Point point2)
        {
            int width = point2.X - point1.X;
            int height = point2.Y - point1.Y;
            Point cornerPoint;

            if (width > 0 && height < 0)
            {                           //quadrant 1
                cornerPoint = new Point(point1.X, point2.Y);
            }
            else if (width < 0 && height < 0)
            {                           //quadrant 2
                cornerPoint = point2;
            }
            else if (width > 0 && height < 0)
            {                           //quadrant 3
                cornerPoint = new Point(point2.X, point1.Y);
            }
            else
            {                           //quadrant 4
                cornerPoint = point1;
            }

            return new Rectangle(cornerPoint.X, cornerPoint.Y, Math.Abs(width), Math.Abs(height));
        }
        private static Bitmap cropImage(Bitmap img, Rectangle cropArea)
        {
            return img.Clone(cropArea, img.PixelFormat);
        }

        private bool doColorsMatch(Color firstColor, Color secondColor, int colorThreshhold)
        {

            /*//assuming second color is the one chosen by hand...
            int userColorOfInterest = 0; //0 = red, 1= green, 2=blue
            int userMaxValue = Math.Max(Math.Max(secondColor.R, secondColor.G), secondColor.B);

            if (userMaxValue == secondColor.R) 
            {              
            }
            else if (userMaxValue == secondColor.G)
            {
                userColorOfInterest = 1;
            }
            else if (userMaxValue == secondColor.B) 
            {
                userColorOfInterest = 2;
            }

            switch (userColorOfInterest) 
            {
                case 0: //r
                    return (Math.Abs(firstColor.R - secondColor.R) < colorThreshhold);//(((firstColor.R - secondColor.G) > 0) && (firstColor.R - secondColor.B > 0));
                case 1: //g
                    return (Math.Abs(firstColor.G - secondColor.G) < colorThreshhold);//(((firstColor.G - secondColor.R) > 0) && (firstColor.G - secondColor.B > 0));
                case 2: //b
                    return (Math.Abs(firstColor.B - secondColor.B) < colorThreshhold);//(((firstColor.B - secondColor.R) > 0) && (firstColor.B - secondColor.G > 0));
            }
            */
            return (Math.Abs(firstColor.R - secondColor.R) < colorThreshhold)  //reds close enough?
                 && (Math.Abs(firstColor.G - secondColor.G) < colorThreshhold)  //greens close enough?
                 && (Math.Abs(firstColor.B - secondColor.B) < colorThreshhold); //blues close enough?
        }
        #endregion

        #region Label Updating
        private void updateStatusLabels(bool p)
        {
            this.status_label.Text = ((p) ? "Running" : "Stopped");
            //this.status_label.BackColor = ((p) ? Color.Green : Color.Red);
            this.status_label.ForeColor = ((p) ? Color.Green : Color.Red);
        }

        private void updateRuntimeLabel(TimeSpan ts)
        {

            this.runtime_label.Text = String.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
        }


        #endregion



        #region File I/O

        private void writeConfigLine(String newConfig)
        {
            //Writing output to file
            try
            {

                StreamWriter sw = new StreamWriter("E:\\Users\\Matt\\Desktop\\" + /*configFileName +*/ ".cfg", true);

                sw.WriteLine(newConfig);


                sw.Close();
            }
            catch (Exception err)
            {
                Console.WriteLine("Exception: " + err.Message);
            }
        }
        private void loadConfig(string loadFileLocation)
        {
            save_context loadedContext = new save_context();
            Match loadedMatch = new Match();
            List<selectedArea> loadedConfig = new List<selectedArea>();

            IFormatter formatter = new BinaryFormatter();
           // XmlSerializer formatter = new XmlSerializer(typeof(save_context));

            try
            {
                Stream stream = new FileStream(loadFileLocation, FileMode.Open, FileAccess.Read, FileShare.Read);

                loadedContext = (save_context)formatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception e) 
            {
                MessageBox.Show("Exception "+ e);
            }

            loadedConfig = loadedContext.finalSelections;
            loadedMatch = loadedContext.finalMatchInfo;
            //set imported data
            exitingSize = loadedContext.finalSizeOfConfigureScreen;
            matchImport = loadedMatch;
            finalSelections = loadedConfig;

        }

        #endregion



    }
}

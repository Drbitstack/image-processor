﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Configure_image_processor
{
    [Serializable]
    public class Tower
    {
        public indexedString status;
        public Tower()
        {
            this.status = new indexedString();
        }
        public Tower(Tower towerClone)
        {
            this.status = towerClone.status;
        }
    }
}
